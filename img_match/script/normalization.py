#-*- coding: utf-8 -*-

import sys
reload(sys)
sys.path.insert(0,'function/')
sys.setdefaultencoding('utf-8')
import pickle
import numpy as np
import h5py
import config
import re
from os import listdir

data_path = '../dataset/items_cat100_224_224.h5'
input_path = '../dataset/Single_B1_B100_name_code.txt'

def load_hdf5_as_numpyarray(path):
	with h5py.File(path) as hf:
		X = hf['items_X'][:]
		Y = hf['items_Y'][:]
		return X,Y

def parse_line(line):
	line = line.strip().split('	')
	code = line[0]
	name = line[1]
	#name = name.decode('big5').encode('utf-8')
	return code, name

def get_data(input_path):
	classes_name = {}
	
	with open(input_path,'r') as f:
		print('Parsing annotation files')
		for line in f:
			print line
			(code, name) = parse_line(line)
			cat = int(code.split('B')[1])-1
			if cat not in classes_name:
				classes_name[cat] = name
		
	return classes_name

classes_name = get_data(input_path)
print classes_name[7]

X_train, y_train = load_hdf5_as_numpyarray(data_path)

X_train = X_train.astype('float32')
X_train_mean = np.mean(X_train, axis=0)
X_train_std = np.std(X_train, axis=0)

print X_train_mean.shape
print X_train_std.shape

C = config.Config()

C.img_channel_mean = X_train_mean
C.std_scaling = X_train_std
C.cat_name = classes_name

config_output_filename = '../cat100_config.pickle'

with open(config_output_filename, 'wb') as config_f:
	pickle.dump(C,config_f)
	print('Config has been written to {}.'.format(config_output_filename))

