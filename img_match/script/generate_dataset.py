import sys
sys.path.insert(0,'function/')
import numpy as np
import cv2
import h5py
from csvDictConverter import csvDictConverter
from folderReader import getfile

#load hdf5 file to numpy array
def load_data(dir):
	with h5py.File(dir,'r') as hf:
		X = hf['10_items_X'][:]
		Y = hf['10_items_Y'][:]
	return X,Y


#convert image to nparray
def imgNparrayConverter(img_dir, dict_labels):
	X = np.array([])
	Y = np.array([])
	imgs = getfile(img_dir)
	for i in range(len(imgs)):
		img = cv2.imread(img_dir+imgs[i])
		img = np.expand_dims(img, axis=0)
		y = np.asarray([[ int(dict_labels[imgs[i]]) ]])
		if X.size > 0:
			X = np.concatenate((X,img))
		else:
			X = img
		if Y.size > 0:
			Y = np.concatenate((Y,y))
		else:
			Y = y
	
	return X,Y

#convert nparray to hdf5 file
def nparrayHdf5Converter(X,Y,dir):
	with h5py.File(dir,'w') as hf:
		hf.create_dataset('X', data=X)
		hf.create_dataset('Y', data=Y)

#give images and labels and transfrom to numpy array
def generate_dataset(processed_img_dir, csv_labels, dataset_dir):
	dict_labels = csvDictConverter(csv_labels)
	X, Y = imgNparrayConverter(processed_img_dir, dict_labels)
	nparrayHdf5Converter(X,Y,dataset_dir)


def generate_all_datasets(processed_img_dir, csv_labels_dir, dataset_dir):
	print 'generating... training dataset'
	generate_dataset(processed_img_dir+'training/', csv_labels_dir+'training.csv',dataset_dir+'training.h5')
	print 'generating... validation dataset'
	generate_dataset(processed_img_dir+'validation/', csv_labels_dir+'validation.csv',dataset_dir+'validation.h5')



