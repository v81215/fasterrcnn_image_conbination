#coding=utf-8
import cv2
import numpy as np
from scipy import ndimage
from skimage import data,segmentation,measure,morphology,color
import math
def gamma_equalization(img, gamma):
	img=img.astype("float32")#轉成小數點作計算
	gamma=float(gamma)
	#print gamma
	maxpixel=np.amax(img)#找最大pixel值
	minpixel=np.amin(img)#找最小pixel值
	#print maxpixel,minpixel	
	img=((img-minpixel)/(maxpixel-minpixel))**gamma*255
	img=img.astype("uint8")#轉回uint8  8bit的矩陣
	return img
def gamma_equalization2(img, gamma):#做兩種gamma
	img=img.astype("float32")#轉成小數點作計算
	gamma=float(gamma)
	#print gamma
	maxpixel=np.amax(img)#找最大pixel值
	minpixel=np.amin(img)#找最小pixel值
	img2=img.copy()	
	
	img2[img>127]=((img[img>127]-128)/(maxpixel-128))**(1/gamma)*127+128
	img2[img<128]=((img[img<128]-0)/(127-0))**gamma*127


	
	img2=img2.astype("uint8")#轉回uint8  8bit的矩陣
	return img2
def gamma_equalization3(img, gamma,mean):#做兩種gamma 用平均值當門檻值
	img=img.astype("float32")#轉成小數點作計算
	gamma=float(gamma)
	#print gamma
	maxpixel=np.amax(img)#找最大pixel值
	minpixel=np.amin(img)#找最小pixel值
	img2=img.copy()	
	
	img2[img>mean]=((img[img>mean]-(255-mean))/(maxpixel-(255-mean)))**(1/gamma)*mean+(255-mean)
	img2[img<(255-mean)]=((img[img<(255-mean)]-0)/(mean-0))**gamma*mean


	
	img2=img2.astype("uint8")#轉回uint8  8bit的矩陣
	return img2

def find_max_label(label):
	#找出最大的label把其餘小label去除掉
	maxarea=0
	height, width=label.shape
	for region in measure.regionprops(label):
		if region.area>maxarea:
			maxarea=region.area
	label=morphology.remove_small_objects(label,min_size=maxarea,connectivity=1) #0是四連通 1是八連通
	'''
	for x in range(0, height):
		for y in range (0,width):
			if label[x,y]!=0:
				label[x,y]=255
	'''
	label[label!=0]=255
	return label

def reverse_type(img): #反白
	height, width=img.shape
	img=img.astype("float32")
	img-=255
	img=abs(img)
	img=img.astype("uint8")
	
	return img
	
def crop_minAreaRect(img, rect):
	angle = rect[2]
	if angle < -45:
		angle += 90
	
	rows,cols = img.shape[0], img.shape[1]
	M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
	img_rot = cv2.warpAffine(img,M,(cols,rows))

	# rotate bounding box
	box = cv2.boxPoints(rect)
	pts = np.int0(cv2.transform(np.array([box]), M))[0]    
	pts[pts < 0] = 0
	
	# crop
	img_crop = img_rot[min(pts[:,1]):max(pts[:,1]), min(pts[:,0]):max(pts[:,0])] #從原圖裁出目標區域img_crop
	return img_crop, box

def sobel(img): #給灰階圖
	x = cv2.Sobel(img,cv2.CV_16S,1,0)
	y = cv2.Sobel(img,cv2.CV_16S,0,1)
	
	absX = cv2.convertScaleAbs(x) # 轉回uint8
	absY = cv2.convertScaleAbs(y)

	dst = cv2.addWeighted(absX,1,absY,1,0)
	return dst

def floodfill(label):#給二值化圖 填滿白色區域（區域內有黑色會被填白）
	label=label.astype("uint8")
	
	im_floodfill = label.copy()
	
	# Mask used to flood filling.
	# Notice the size needs to be 2 pixels than the image.
	h, w = label.shape[:2]
	mask = np.zeros((h+2, w+2), np.uint8)
	
	# Floodfill from point (0, 0)
	cv2.floodFill(im_floodfill, mask, (0,0), 255);

	# Invert floodfilled image
	im_floodfill_inv = cv2.bitwise_not(im_floodfill)
	
	# Combine the two images to get the foreground.
	im_out = label | im_floodfill_inv
	
	return im_out

def strokeEdges(src, dst, blurKsize = 7, edgeKsize = 5):
	if blurKsize >= 3:
		blurredSrc = cv2.medianBlur(src, blurKsize)
		graySrc = cv2.cvtColor(blurredSrc, cv2.COLOR_BGR2GRAY)
	else:
		graySrc = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
	cv2.Laplacian(graySrc, cv2.CV_8U, graySrc, ksize = edgeKsize)
	
	normalizedInverseAlpha = (1.0/255) * (graySrc)
	cv2.imwrite("test345.jpg",normalizedInverseAlpha)
	channels = cv2.split(src)
	for channel in channels:
		channel[:] = channel * normalizedInverseAlpha
	cv2.merge(channels, dst)
	return dst

def rectResize(rect, n):
	((x, y), (w, h), degree) = rect
	return ((x*n, y*n), (w*n, h*n), degree)

#pad input image into square
def pad(img):
	(rows,cols,_) = img.shape
	m = max(rows,cols)
	upper = (m-rows)/2
	lower = m-((m-rows)/2)-rows
	left = (m-cols)/2
	right = m-((m-cols)/2)-cols
	mode = cv2.BORDER_CONSTANT
	pad_img = cv2.copyMakeBorder(img,upper,lower,left,right, mode, value=[0,0,0])
	return pad_img


