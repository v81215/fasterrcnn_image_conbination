from keras.layers import concatenate
from keras.layers.core import Lambda
from keras.models import Model

import tensorflow as tf

def make_parallel(model, gpu_list):
	def get_slice(data, idx, parts):
		shape = tf.shape(data)
		size = tf.concat([ shape[:1] // parts, shape[1:] ], axis=0)
		stride = tf.concat([ shape[:1] // parts, shape[1:]*0 ], axis=0)
		start = stride * idx
		return tf.slice(data, start, size)

	outputs_all = []
	for i in range(len(model.outputs)):
		outputs_all.append([])
	
	#Place a copy of the model on each GPU, each getting a slice of the batch
	gpu_count = len(gpu_list)
	for i in range(gpu_count):
		with tf.device('/gpu:%d' % gpu_list[i]):
			with tf.name_scope('tower_%d' % gpu_list[i]) as scope:
				inputs = []
				#Slice each input into a piece for processing on this GPU
				for x in model.inputs:
					input_shape = tuple(x.get_shape().as_list())[1:]
					slice_n = Lambda(get_slice, output_shape=input_shape, arguments={'idx':i,'parts':gpu_count})(x)
					inputs.append(slice_n)

					outputs = model(inputs)
					
					if not isinstance(outputs, list):
						outputs = [outputs]
					
					#Save all the outputs for merging back together later
					for l in range(len(outputs)):
						outputs_all[l].append(outputs[l])
	
	# merge outputs on CPU
	with tf.device('/cpu:0'):
		merged = []
		for outputs in outputs_all:
			merged.append(concatenate(outputs, axis=0))
	
		return Model(inputs=model.inputs, outputs=merged)

'''
if __name__ == "__main__":
	from keras.models import Model
	from keras.layers import Input, Dense, Dropout, Activation, Flatten, BatchNormalization
	from keras.layers.advanced_activations import LeakyReLU
	from keras.preprocessing.image import ImageDataGenerator
	from keras.optimizers import SGD
	from keras.callbacks import ModelCheckpoint
	from keras.utils import to_categorical
	from keras.applications.vgg16 import VGG16
	from keras.applications.vgg16 import preprocess_input
	import numpy as np
	from os import listdir
	from os.path import isfile, join
	import sys
	import item_dataset
	from datetime import datetime

	batch_size = 48
	nb_category = 1013
	drop_out = 0.4
	sample = 6
	lrelu = LeakyReLU(alpha = 0.1)

	input_shape = (224,224,3)
	
	(X_test, y_test) = item_dataset.load_train_without_split('test_drink_cat1013_gt_224')
	
	X_test = X_test.astype('float32')
	X_test -= np.mean(X_test, axis=0)
	X_test /= np.std(X_test, axis=0)
	
	print X_test.shape
	
	path = 'model/_items_cat1013_gt_val_test_fine_tune/'
	modelList = [m for m in listdir(path)]
	modelList.sort()

	def createGenerator(X,Y):
		while True:
			datagen = ImageDataGenerator(
					featurewise_center=True,
					featurewise_std_normalization=True,
					rotation_range=180,
					width_shift_range=0.1,
					height_shift_range=0.1)
			datagen.fit(X_train)
			batches = datagen.flow(X,Y,batch_size=batch_size)
			for b in batches:
				yield b[0], b[1]
	
	def VGG16Net():
		model_vgg16_conv = VGG16(weights='imagenet', include_top=False)
		inputs = Input(shape=input_shape, name='input')
		model = model_vgg16_conv(inputs)
		x = Flatten()(model)
		cat = Dense(4096, name='fc6_category')(x)
		cat = LeakyReLU()(cat)
		cat = Dropout(drop_out)(cat)
		cat = Dense(4096, name='fc7_category')(cat)
		cat = LeakyReLU()(cat)
		cat = Dropout(drop_out)(cat)
		cat = Dense(nb_category, activation='softmax', name=str(nb_category)+'_category')(cat)
		return inputs, cat
	
	inputs,cat = VGG16Net()
	model = Model(inputs=inputs, outputs=cat)
	model.summary()

	parallel_model = make_parallel(model , [0,1])

	print modelList[0]
		
	parallel_model.load_weights(path+modelList[0], by_name=True)
	parallel_model.summary()
	my_model = parallel_model.layers[-2]
	
	my_model.save(path+'drink-zero-centered-load-000-0.980.h5')
'''
