# -*- coding: utf-8 -*-
#!/usr/bin/env python
import time
import math
import cv2, sys, numpy, os
import subprocess
import numpy as np

def read_transparent_png(filename):
    image_4channel = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
    alpha_channel = image_4channel[:,:,3]
    rgb_channels = image_4channel[:,:,:3]

    # White Background Image
    white_background_image = np.ones_like(rgb_channels, dtype=np.uint8) * 0

    # Alpha factor
    alpha_factor = alpha_channel[:,:,np.newaxis].astype(np.float32) / 255.0
    alpha_factor = np.concatenate((alpha_factor,alpha_factor,alpha_factor), axis=2)
    # Transparent Image Rendered on White Background
    base = rgb_channels.astype(np.float32) * alpha_factor
    white = white_background_image.astype(np.float32) * (1 - alpha_factor)
    final_image = base + white

    return final_image.astype(np.uint8)

def check_box_success(center,img,index):
    #white_background_image = np.ones_like(img, dtype=np.uint8) * 0
    if index == 9:
        #image_checkbox = read_transparent_png("checkbox_icon2/checkbox_"+str(index+1)+".png")
        image_checkbox = cv2.imread("checkbox_icon2/checkbox_"+str(index+1)+".jpg")
    else :
        #image_checkbox = read_transparent_png("checkbox_icon2/checkbox_0"+str(index+1)+".png")
        image_checkbox = cv2.imread("checkbox_icon2/checkbox_"+str(index+1)+".jpg")
    image_checkbox_resize = cv2.resize(image_checkbox, (90, 90))
    for x in range(90):
        for y in range(90):
            ix = center[0]-45 + x
            iy = center[1]-45 + y
            if image_checkbox_resize[y,x][0] != 255 and image_checkbox_resize[y,x][1] != 255 and image_checkbox_resize[y,x][2] != 255:
                img[iy,ix] = image_checkbox_resize[y,x]

def check_box_success2(center,img,index):
    #white_background_image = np.ones_like(img, dtype=np.uint8) * 0
    image_checkbox = cv2.imread("new_check/number_0"+str(index+1)+".png")
    image_checkbox_resize = cv2.resize(image_checkbox, (100, 100))
    for x in range(100):
        for y in range(100):
            ix = center[0]-50 + x
            iy = center[1]-50 + y
            #if image_checkbox_resize[y,x][0] != 255 and image_checkbox_resize[y,x][1] != 255 and image_checkbox_resize[y,x][2] != 255:
            img[iy,ix] = image_checkbox_resize[y,x]

def check_box_success3(center,img,index,number):
    #white_background_image = np.ones_like(img, dtype=np.uint8) * 0
    if index ==1:
        image_checkbox = cv2.imread("Number_ok/number_0"+str(number)+".png")
        print "Number_ok/number_0"+str(number)+".png"
    else:
        image_checkbox = cv2.imread("Number_ok/number_0"+str(number)+"a"+str(index)+".png")
    image_checkbox_resize = cv2.resize(image_checkbox, (80, 80))
    img[center[1]-40:center[1]+40,center[0]-40:center[0]+40] = image_checkbox_resize
    #for x in range(100):
    #    for y in range(100):
     #       ix = center[0]-50 + x
      #      iy = center[1]-50 + y
            #if image_checkbox_resize[y,x][0] != 255 and image_checkbox_resize[y,x][1] != 255 and image_checkbox_resize[y,x][2] != 255:
       #     img[iy,ix] = image_checkbox_resize[y,x]

def check_box_error(center,img,index,number):
    #white_background_image = np.ones_like(img, dtype=np.uint8) * 0
    if index ==1:
        image_checkbox = cv2.imread("Number_error/number_error_0"+str(number)+".png")
        print "Number_ok/number_0"+str(number)+".png"
    else:
        image_checkbox = cv2.imread("Number_error/number_error_0"+str(number)+"a"+str(index)+".png")
    image_checkbox_resize = cv2.resize(image_checkbox, (80, 80))
    img[center[1]-40:center[1]+40,center[0]-40:center[0]+40] = image_checkbox_resize
    #for x in range(100):
     #   for y in range(100):
      #      ix = center[0]-50 + x
       #     iy = center[1]-50 + y
            #if image_checkbox_resize[y,x][0] != 255 and image_checkbox_resize[y,x][1] != 255 and image_checkbox_resize[y,x][2] != 255:
        #    img[iy,ix] = image_checkbox_resize[y,x]
