#coding=utf-8
import cv2
import os
from os.path import isfile, join
from os import listdir
import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport exp


DTYPE = np.double
ctypedef np.double_t DTYPE_t
TTYPE = np.uint8
ctypedef np.uint8_t TTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)

cpdef runlength(np.ndarray[TTYPE_t,ndim=2] gray):


	cdef np.ndarray[TTYPE_t,ndim=2] runlengthp = gray.copy()
	cdef int window=5
	cdef int W=(window*2)+1
	cdef int h=gray.shape[0]
	cdef int w=gray.shape[1]
	cdef int i,j,top,down,left,right,xSi,ySi,nowx,nowy,block_h,block_w
	cdef float block_sum,block_mean,Max,Min
	cdef np.ndarray[DTYPE_t,ndim=2] line8
	cdef np.ndarray[TTYPE_t,ndim=2] block
	cdef np.ndarray[double,ndim=2] step=np.array([[1,0,1],[2,1,0.5],[1,1,1],[1,2,0.5],[0,1,1],[-1,2,0.5],[-1,1,1],[-2,1,0.5]])
	
	for i in xrange(h):
		top = i - window
		if top < 0:
			top = 0        	       
		down = i + window
		if down > h:
			down =h-1
		xSi = int((down+1-top)/2)
		for j in xrange(w):
			
        	       
			left = j-window
			if left < 0:
				left = 0
        
			right = j+window
			if right > w:
				right = w-1
			block=gray[top:down+1,left:right+1]
			block_h=block.shape[0]
			block_w=block.shape[1]
			
			
			ySi = int((right+1-left)/2)
			line8 =np.zeros((8,2),dtype=float)
			
			for inneri in xrange (8):
				for innerj in xrange(2):
					nowx = 0
					nowy = 0
					while 1:
						if(nowx+xSi<0)or(nowx+xSi>block.shape[0])or(nowy+ySi<0)or(nowy+ySi>block.shape[1]):
							break
						line8[inneri,0] = line8[inneri,0]+ block[(nowx+xSi),(nowy+ySi)]
						line8[inneri,1] = line8[inneri,1] + 1
						nowx =int(nowx + step[inneri,0])
						nowy =int(nowy + step[inneri,1])
        	               			
					step[inneri,0] = -step[inneri,0]
					step[inneri,1] = -step[inneri,1]
			Max=0
			Min=100000000000
			for a in xrange(8):
				if Max<(line8[a,0] / line8[a,1]):
					Max=line8[a,0] / line8[a,1]
				if Min>(line8[a,0] / line8[a,1]):
					Min=line8[a,0] / line8[a,1]
			block_sum=0
			for b in xrange(block_h):
				for c in xrange(block_w):
					block_sum+=block[b,c]
				
			block_mean=block_sum/(block_h*block_w)
			
			if gray[i,j]>block_mean:
				runlengthp[i,j] = int(Max)
			elif gray[i,j]<block_mean:
				runlengthp[i,j] = int(Min)
			else:
				runlengthp[i,j] = gray[i,j]
       	
	return runlengthp
      	
	
		
