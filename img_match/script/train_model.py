import sys
sys.path.insert(0, 'script/')
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD, Adam, RMSprop
from keras.callbacks import ModelCheckpoint


def createGenerator(X, Y, batch_size):
	while True:
		datagen = ImageDataGenerator(
				featurewise_center=True,
				featurewise_std_normalization=True,
				rotation_range=180,
				width_shift_range=0.1,
				height_shift_range=0.1)
		datagen.fit(X)
		batches = datagen.flow(X,Y,batch_size=batch_size)
		for b in batches:
			yield b[0], b[1]

def train_model(data, model, checkpoint_dir,
		loss='categorical_crossentropy', optimizer='sgd', 
		batch_size=32, sample=6, epoch=500, lr=0.001, sgd_momentum=0.9):
	
	X_train = data[0][0]
	Y_train = data[0][1]
	X_val = data[1][0]
	Y_val = data[1][1]
	file_path = checkpoint_dir+'model-{epoch:03d}-{val_acc:.3f}.h5'
	
	
	if optimizer=='sgd':
		opt = SGD(lr=lr,decay=lr/epoch, momentum=sgd_momentum)
	elif optimizer == 'adam':
		opt=Adam(lr=lr)
	elif optimizer == 'rmsprop':
		opt=RMSprop(lr=lr)
	
	model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])
	callbacks_list = [ModelCheckpoint(file_path, monitor='val_acc', verbose=0, save_best_only=True, mode='max')]


	history = model.fit_generator(createGenerator(X_train,Y_train,batch_size), 
			samples_per_epoch=X_train.shape[0]*sample, 
			nb_epoch=epoch,
			validation_data=(X_val,Y_val),
			callbacks=callbacks_list)


