import zerorpc


class UIConnector():

    def __init__(self):
        self.client = zerorpc.Client()
        self.client.connect("tcp://127.0.0.1:4242")

    def onDetectItem(self,item_list):
        self.client.onItemListUpdated(item_list)

    def onExited(self):
        self.client.onItemListUpdated([])