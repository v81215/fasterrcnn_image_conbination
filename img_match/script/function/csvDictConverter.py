import csv

#transform csv file to dictionary
#csv_file:string (path to csv file), return:dictionary
def csvDictConverter(csv_file):
	reader = csv.DictReader(open(csv_file,'rb'))
	dict_list = []
	for line in reader:
		dict_list.append(line)
	return dict_list[0]
