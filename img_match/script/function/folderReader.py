from os.path import isfile, isdir, join
from os import listdir

#get input directory's sub-directory
#dir:string return:list
def getsubdir(dir):
	sub_dir = [d for d in listdir(dir) if isdir(join(dir,d))]
	return sub_dir


#get files from intput directory
#dir:string, return: list
def getfile(dir,isSort=True):
	files = [f for f in listdir(dir) if isfile(join(dir,f))]
	if isSort:
		files.sort()
		return files
	else:
		return files
