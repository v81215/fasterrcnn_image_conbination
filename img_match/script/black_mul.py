#coding=utf-8
import cv2
import os
import math
from os.path import isfile, join
from datetime import datetime
from os import listdir
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
import function
from skimage import data,segmentation,measure,morphology,color
from skimage.measure import regionprops
import re
from skimage.filters import threshold_otsu
import runlength
#from skimage.filters.rank import entropy
from skimage.morphology import disk,watershed	
#from skimage.feature import peak_local_max
from skimage.filters import rank
from skimage.util import img_as_ubyte
from skimage.filters import rank
import thinning

#止滑墊當背景,長寬各縮為1/8倍
#path = 'multiple_product/'
#path = 'IOU/items_testdone/'
#path = 'IOU/difficult/'
#path='test/validation/'
#path='test/train/'
#path='test/test2/'
path='black_mul_product/'

folder = os.listdir(path)
folder.sort()

count = 0
total_bbox = 0
#init_time = datetime.now()


for file in folder:
	init_time = datetime.now()
	img=cv2.imread(path+file,-1)
	big_raw=img.copy()
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	

	
	img = cv2.resize(img, (img.shape[1]/9, img.shape[0]/9))
	gray = cv2.resize(gray, (gray.shape[1]/9, gray.shape[0]/9))
	height, width=gray.shape
	
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_0_Raw.jpg',img)
	raw=img.copy()
	img_otsu=img.copy() 
	
	img=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
	
	
	R = img_otsu[:,:,0]
	B = img_otsu[:,:,1]
	G = img_otsu[:,:,2]
	
	H= img[:,:,1]

	V = img[:,:,2]
	V = V*255
	V = 255-V
	


	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_1_Gaussian.jpg',H)
	otsu1,th1 = cv2.threshold(R,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	otsu2,th2 = cv2.threshold(B,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	otsu3,th3 = cv2.threshold(G,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	otsu4,th4 = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	
	R=function.gamma_equalization3(R,1,otsu1)
	B=function.gamma_equalization3(B,1,otsu2)
	G=function.gamma_equalization3(G,1,otsu3)
	V=function.gamma_equalization3(V,1,otsu4)
	V2=function.gamma_equalization(V,1)
	
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_1_Gaussian.jpg',V)
	
	
	#高斯模糊
	#R = cv2.GaussianBlur(R,(7,7),3)
	#B = cv2.GaussianBlur(B,(7,7),3)
	#G = cv2.GaussianBlur(G,(7,7),3)
	#img2= cv2.GaussianBlur(V,(7,7),3)
	img2=V
	
	

	R=function.sobel(R)
	B=function.sobel(B)
	G=function.sobel(G)
	dst2=function.sobel(img2)

	R=function.gamma_equalization(R,1)
	B=function.gamma_equalization(B,1)
	G=function.gamma_equalization(G,1)
	dst2=function.gamma_equalization(dst2,1)
	

	
	avgR,th1 = cv2.threshold(R,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	avgB,th2 = cv2.threshold(B,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	avgG,th3 = cv2.threshold(G,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	avgV,th4 = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	if  np.mean(gray)>95:
		thred=300	
	
		avgR=70
		avgB=70
		avgG=70
		avgV=90


	elif np.mean(gray)>85:
		thred=500	
	
		avgR=70
		avgB=70
		avgG=70
		avgV=80



	elif np.mean(gray)>75:
		thred=500	
	
		avgR=70
		avgB=70
		avgG=70
		avgV=70
		
	
	elif np.mean(gray)>50:
		thred=40	
		avgR=50
		avgB=50
		avgG=50
		avgV=65
	
	elif np.mean(gray)>30:
		thred=30	
		avgR=50
		avgB=50
		avgG=50
		avgV=50
		

	else:
		thred=10	
		avgR=30
		avgB=30
		avgG=30
		avgV=30
		
		
	
	print file , np.mean(gray),avgV
	R[R<avgR]=0
	B[B<avgB]=0
	G[G<avgG]=0
	
	dst2[dst2<avgV]=0
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_3_totalsobel.jpg',dst2)
	'''
	dst2 = cv2.add(dst2,R)
	#dst2 = cv2.add(dst2,R)
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_3_totalsobel+R.jpg',dst2)
	dst2 = cv2.add(dst2,B)
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_3_totalsobel+RB.jpg',dst2)
	#dst2[dst2<avgV]=0
	dst2 = cv2.add(dst2,G)
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_3_totalsobel+RBG.jpg',dst2)
	'''
	#dst2=function.gamma_equalization(dst2,2)
	dst2=np.insert(dst2, height, 0, axis=0)
	dst2=np.insert(dst2, width, 0, axis=1)
	dst2=np.insert(dst2, 0, 0, axis=0)
	dst2=np.insert(dst2, 0, 0, axis=1)
	dst2=runlength.runlength(dst2)
	dst2=dst2[1:height+1, 1:width+1]
	#dst2=function.gamma_equalization(dst2,2)
	#cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_3_totalsobel_run.jpg',dst2)
	
	dst2[dst2<50]=0
	dst2[dst2>0]=255
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_3_totalsobel_white.jpg',dst2)
	
	
	
	
	labeled_array1, num_features1 = ndimage.measurements.label(dst2) #取label
	
	small_region=[]
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)
	
	small_region=np.mean(small_region)
	print small_region
	
	
	labeled_array1=morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1) #0是四連通 1是八連通
	labeled_array1[labeled_array1>0]=255
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_4_remove_small_objects.jpg',labeled_array1)
	kernel = np.ones((3,3),np.uint8) #mask
	kernel=kernel.astype("uint8")
	#labeled_array1 = cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations = 1) #dilation 白色區域-膨脹
	
	
	
	#整張圖外圍加一層邊界
	labeled_array1=np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1=np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1=np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1=np.insert(labeled_array1, 0, 0, axis=1)
		
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_5_dilation.jpg',labeled_array1)
	
	im_out=function.floodfill(labeled_array1)

	im_out=im_out[1:height+1, 1:width+1]
	

	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_6_floodfill.jpg',im_out)

	im_out, num_features = ndimage.measurements.label(im_out)
	
	im_out =morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1) #0是四連通 1是八連通
	 

	labeled_array1 = cv2.dilate(im_out.astype(np.float32),kernel,iterations = 1) #dilation 白色區域膨脹
	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test=[]
	for region in measure.regionprops(labeled_array1):
		value=region.area/region.perimeter**2
		#print file.split('.')[0],value
		if value<0.008:
			test.append(region.label)

	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]]=0
	labeled_array1 =morphology.remove_small_objects(labeled_array1,min_size=height*width*0.02,connectivity=1) #0是四連通 1是八連通
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0]=255
	
	
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_7_removelongarea.jpg',im_out)
	im_out=np.insert(im_out, height, 0, axis=0)
	im_out=np.insert(im_out, width, 0, axis=1)
	im_out=np.insert(im_out, 0, 0, axis=0)
	im_out=np.insert(im_out, 0, 0, axis=1)
	im_out= cv2.erode(im_out.astype(np.float32),kernel,iterations = 2) #erosion 白色區域侵蝕5*5
	
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations = 2) #dilation 白色區域膨脹
	
	#kernel = np.ones((3,3),np.uint8)
	#im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations = 1) #dilation 白色區域膨脹	
	
	'''
	im_out= (im_out.astype(np.float32),kernel,iterations = 1) #erosion 白色區域侵蝕3*3
	'''
	im_out=im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	
	im_out =morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1) #0是四連通 1是八連通



	im_out[im_out>0]=255
	cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_8_erosion.jpg',im_out)
	
	#整張圖外圍加一層邊界
	im_out=np.insert(im_out, height, 0, axis=0)
	im_out=np.insert(im_out, width, 0, axis=1)
	im_out=np.insert(im_out, 0, 0, axis=0)
	im_out=np.insert(im_out, 0, 0, axis=1)
		
	
	
	im_out=function.floodfill(im_out)

	im_out=im_out[1:height+1, 1:width+1]


	im_out, num_features = ndimage.measurements.label(im_out)
	test2=[]
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2	
		#print file.split('.')[0],value
		
		if value<0.024:
			test2.append(region.label)

	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	im_out[im_out>0]=255
	
	#邊界各增加70 避免旋轉後裁切到商品
	
	im_out = cv2.copyMakeBorder(im_out, 70, 70, 70,70, cv2.BORDER_CONSTANT, value=0)
	big_raw = cv2.copyMakeBorder(big_raw,630, 630, 630,630, cv2.BORDER_CONSTANT, value=[0,0,0])
	#放大為原始大小
	#big_size=np.zeros((big_raw.shape[0], big_raw.shape[1]))
	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	#cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_9_result.jpg',im_out) #存檔
	
	objcnt = 0
	for cnt in conts:
		objcnt += 1  #計次
		rect = cv2.minAreaRect(cnt) #記座標
		((x,y),(w,h),degree)=rect
		rect=((x*9,y*9),(w*9,h*9),degree)
		box,img_crop, pts = function.crop_minAreaRect(big_raw,rect) #裁出物體矩形
		box=box*9
		#cv2.imwrite('Items_Dataset/Img/testing_crop14/'+file.split('.')[0]+'_'+str(objcnt)+'.jpg',img_crop) #存檔名
		cv2.imwrite('Items_Dataset/Img/testing_crop13/'+file.split('.')[0]+'_9_0_'+str(objcnt)+'_result_.jpg',img_crop) #存檔名
