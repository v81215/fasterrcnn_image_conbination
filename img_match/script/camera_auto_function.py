#coding=utf-8
import numpy as np
import cv2
import os
import string
import ConfigParser
import json
from datetime import datetime
from transform import four_point_transform
import segfunction

def exposure(frames,param,finetune):
	config = ConfigParser.ConfigParser()
	config.read('output/bounding.ini')
	ix=json.loads(config.get("coordinate","x"))
	iy=json.loads(config.get("coordinate","y"))
	area = np.array(([[ix[0],iy[0]],[ix[1],iy[1]],[ix[2],iy[2]],[ix[3],iy[3]]]), dtype = "float32")
	warped = four_point_transform(frames[5], area)
	gray_image = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
	
	if gray_image.mean()<22 and param<1000:
		param+=100
	
	elif gray_image.mean()>28 and param>200:
		param-=100

	if gray_image.mean()<24 and param<1000:
		param+=5
	elif gray_image.mean()>26 and param>200:
		param-=5
	
	if gray_image.mean()>24 and gray_image.mean()<26:
		finetune=False
		bounding_set(frame)
	return param,finetune


def bounding_set(frame):
	config = ConfigParser.ConfigParser()
	config.read('output/bounding.ini')
	ix=json.loads(config.get("coordinate","x"))
	iy=json.loads(config.get("coordinate","y"))
	area = np.array(([[ix[0],iy[0]],[ix[1],iy[1]],[ix[2],iy[2]],[ix[3],iy[3]]]), dtype = "float32")
	warped = four_point_transform(frame, area)
	bg=segfunction.background(warped)
	cv2.imwrite('output/bg_test.jpg',bg)
if __name__ == '__main__':
	frames=[]
	finetune=True
	print cv2.__version__
	capture = cv2.VideoCapture(0)
	capture.set(3,3840)
	capture.set(4,2160)

	os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=exposure_auto=1")
	os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=exposure_auto_priority=0")
	os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=exposure_absolute=600")
	os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=focus_auto=0")
	os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=focus_absolute=0")
	os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=zoom_absolute=100")
	param=600
	
	
	
	
	while(1):
		
		ret, frame = capture.read()
		show_frame=cv2.resize(frame,(1920,1080))
		cv2.imshow("frame", show_frame)
		
		print len(frames),param,finetune
		if finetune:
			frames.append(frame)
			if len(frames)>5:
				param,finetune=exposure(frames,param,finetune)
				os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=exposure_auto=1")
				os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=exposure_auto_priority=0")
				os.system("v4l2-ctl --device=/dev/video0 --set-ctrl=exposure_absolute=%s" % (param))
				frames=[]
		if cv2.waitKey(10) & 0xFF == ord('t'):
			print ('take picture')
			
			cv2.imwrite('Img_'+str(param)+'_'+str(int(gray_image_area.mean()))+'_'+str(datetime.now())+'.jpg', frame)
	
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
		

