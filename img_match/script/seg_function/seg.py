import cv2
import numpy as np
import os
from os.path import isfile, join
from datetime import datetime
from os import listdir
import preprocess_image

path='webcam_img/'
folder = os.listdir(path)
folder.sort()
print len(folder)
bg_img=cv2.imread('boouding.jpg',0)
for file in folder:
	print file
	init_time = datetime.now()

	#load img
	raw=cv2.imread(path+file)
	
	gray=cv2.cvtColor(raw, cv2.COLOR_BGR2GRAY)
	cropped_images, bboxes, centroids, rect_data, item_text=preprocess_image.object_proposal_edge_black_v2(raw,bg_img,file)
	
	for x in range(len(bboxes)):
		cv2.polylines(raw, np.int32([bboxes[x]]),True, (255,255,255))
		cv2.imwrite(file,raw)
	#for x in range (len(cropped_images)):
		#cv2.imwrite(file,cropped_images[x])
		#cv2.imwrite('seg_result/'+file+'_'+str(x)+'.jpg',cropped_images[x])
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	
