#coding=utf-8
import cv2
import os
import math
from os.path import isfile, join
from datetime import datetime
from os import listdir
import numpy as np
from scipy import ndimage
import function
from skimage import data,segmentation,measure,morphology,color
from skimage.measure import regionprops
import re
from skimage.filters import threshold_otsu
import runlength


img=cv2.imread('background/P6_Img_0.jpg')
img=img[150:870,450:1410]#4:3
gray=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
height, width=gray.shape
print img.shape

V = img[:,:,2]
V = V*255
V = 255-V

V=cv2.equalizeHist(V)#直方圖等距拉開
cv2.imwrite('_5_result_6plus.jpg',V) #存檔名
otsu4,th4 = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)



V=function.gamma_equalization3(V,1,otsu4)
V2=function.gamma_equalization(V,2)


img2=V




dst2=function.sobel(img2)


dst2=function.gamma_equalization(dst2,1)

cv2.imwrite('_5_result_6plus_.jpg',dst2)


avgV,th4 = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)



dst2[dst2<avgV+90]=0
dst2[dst2>=avgV+90]=255
cv2.imwrite('_5_result_6plus_1.jpg',dst2)
dst2=np.insert(dst2, height, 0, axis=0)
dst2=np.insert(dst2, width, 0, axis=1)
dst2=np.insert(dst2, 0, 0, axis=0)
dst2=np.insert(dst2, 0, 0, axis=1)
dst2=runlength.runlength(dst2)
dst2=dst2[1:height+1, 1:width+1]
kernel = np.ones((3,3),np.uint8) #mask

dst2[dst2<50]=0
dst2[dst2>0]=255


labeled_array1, num_features1 = ndimage.measurements.label(dst2)
labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=200,connectivity=1)
labeled_array1[labeled_array1>0] = 255

cv2.imwrite('_5_result_6plus_2.jpg',labeled_array1)


dst2 = cv2.dilate(dst2.astype(np.float32),kernel,iterations = 1) #dilation 白色區域膨脹
dst2 = cv2.erode(dst2.astype(np.float32),kernel,iterations = 1)


labeled_array1, num_features1 = ndimage.measurements.label(dst2) #取label
labeled_array1=function.find_max_label(labeled_array1)
	



cv2.imwrite('_6_result_6plus.jpg',labeled_array1) #存檔名


labeled_array1=function.floodfill(labeled_array1)
labeled_array1= cv2.erode(labeled_array1.astype(np.float32),kernel,iterations = 4) 
labeled_array2= cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations = 5) 
cv2.imwrite('_6_result_6plus_2.jpg',labeled_array1)
labeled_array2[labeled_array1==255]=0
cv2.imwrite('boouding.jpg',labeled_array2)


