import sys
import numpy as np
sys.path.insert(0,'script/')

from keras.utils import np_utils
from generate_dataset import load_data

def preprocess_dataset(dir, category):
	#load dataset
	X, y = load_data(dir)
	#transfrom type
	X = X.astype('float32')
	#normalize
	X -= np.mean(X,axis=0)
	X /= np.std(X,axis=0)
	#transfrom to one-hot format
	Y = np_utils.to_categorical(y, category)
	return X, Y

def preprocess_all_dataset(dir, category):
	print 'processing... [%s]' %(dir+'training.h5')
	X_train, Y_train = preprocess_dataset(dir+'training.h5', category)
	print 'processing... [%s]'%(dir+'validation.h5')
	X_val, Y_val = preprocess_dataset(dir+'validation.h5', category)
	return (X_train, Y_train), (X_val, Y_val)
