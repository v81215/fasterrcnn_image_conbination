import sys
sys.path.insert(0,'script/')

from keras.models import Model
from keras.layers import Input, Dense, Dropout, Flatten
from keras.layers.advanced_activations import LeakyReLU
from keras.utils import np_utils
from keras.applications.vgg16 import VGG16



def ResNet(intput_shape, category, drop_out):
	return None

#generate VGG16 net
def VGG16Net(input_shape, category, drop_out):
	model_vgg16_conv = VGG16(weights='imagenet', include_top=False)
	inputs = Input(shape=input_shape, name='input')
	model = model_vgg16_conv(inputs)
	x = Flatten()(model)
	cat = Dense(1024, name='fc6_category')(x)
	cat = LeakyReLU()(cat)
	cat = Dropout(drop_out)(cat)
	cat = Dense(1024, name='fc7_category')(cat)
	cat = LeakyReLU()(cat)
	cat = Dropout(drop_out)(cat)
	cat = Dense(category, activation='softmax', name='category')(x)
	return inputs, cat

#choose model
def create_model(input_shape, category, drop_out, net='vgg16'):

	if net == 'vgg16':
		inputs, cat = VGG16Net(input_shape, category, drop_out)
	elif net == 'resnet':
		inputs, cat = ResNet(input_shape, category, drop_out)
	
	else:
		return None
	
	
	print 'loading... [%s]' %(net)
	model = Model(input=inputs, output=cat)
	return model


