#coding=utf-8
import sys
import cv2
import csv
import math
import numpy as np
import re
import runlength
import os
from scipy import ndimage
from skimage import data,segmentation,measure,morphology,color
from skimage.measure import regionprops
from skimage.filters import threshold_otsu
from skimage.filters.rank import entropy
from skimage.morphology import disk
import function.imageProcessor as imgPcsr
from datetime import datetime 

poposal_time=datetime.now()

def background(object_img):
	
	
	now_time=datetime.now()
	gray_img=cv2.cvtColor(object_img, cv2.COLOR_BGR2GRAY)

	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	if raw_width==960:
		print 960
		object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
		gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	elif raw_width==1920:
		object_img = cv2.resize(object_img, (raw_width/6, raw_height/6))
		gray_img = cv2.resize(gray_img, (raw_width/6, raw_height/6))
	else :
		object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
		gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))
	
	otsu_img = object_img.copy()
	height, width = gray_img.shape
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)

	V = object_img[:,:,2]

	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	

	

	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	
	V = np.insert(V, height, 25, axis=0)
	V = np.insert(V, width, 25, axis=1)
	V = np.insert(V, 0, 25, axis=0)
	V = np.insert(V, 0, 25, axis=1)
	
	
	V_sobel = imgPcsr.sobel(V)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	#把sobel圖 對比拉開造成只有背景的邊框太強難以消除 故不使用 
	#V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
	
	if np.mean(gray_img)>95:
		thred=500	
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500	
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500	
		avgV=70
	elif np.mean(gray_img)>50:
		thred=100
		avgV=45
	elif np.mean(gray_img)>30:
		thred=100
		avgV=45
	else:
		thred=10
		avgV=30
	
	kernel = np.ones((3,3),np.uint8)
	kernel = kernel.astype("uint8")

	V_sobel[V_sobel<avgV] = 0
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel_border = V_sobel.copy()
	V_sobel = V_sobel_border[1:height+1, 1:width+1]

	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)

	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)

	small_region = np.mean(small_region)

	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255
	return labeled_array1



def seg(raw,bg_img):

	global poposal_time
	
	init_time = datetime.now()
	
	name=''
	object_img=raw
	gray_img=cv2.cvtColor(object_img, cv2.COLOR_BGR2GRAY)
	
	bg_img[bg_img > 175]=255
	bg_img[bg_img < 175]=0


	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	if raw_width==960:
		print 960
		object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
		gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	elif raw_width==1920:
		object_img = cv2.resize(object_img, (raw_width/6, raw_height/6))
		gray_img = cv2.resize(gray_img, (raw_width/6, raw_height/6))
	else :
		object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
		gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))
	#print object_img.shape,bg_img.shape
	otsu_img = object_img.copy()
	cv2.imwrite('seg_result/'+str(name)+'0.raw.jpg',object_img)
	height, width = gray_img.shape
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)

	V = object_img[:,:,2]

	cv2.imwrite('seg_result/'+str(name)+'1.V.jpg',V)
	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	

	

	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	cv2.imwrite('seg_result/'+str(name)+'2.gamma.jpg',V)
	print V.shape[0],V.shape[1]
	print height
	V = np.insert(V, height, 25, axis=0)
	V = np.insert(V, width, 25, axis=1)
	V = np.insert(V, 0, 25, axis=0)
	V = np.insert(V, 0, 25, axis=1)
	
	
	V_sobel = imgPcsr.sobel(V)
	cv2.imwrite('seg_result/'+str(name)+'2.1sobel.jpg',V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	
	cv2.imwrite('seg_result/'+str(name)+'3.sobel.jpg',V_sobel)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
	cv2.imwrite('seg_result/'+str(name)+'4.sobel+gamma.jpg',V_sobel)
	
	if np.mean(gray_img)>95:
		thred=500	
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500	
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500	
		avgV=70
	elif np.mean(gray_img)>50:
		thred=100
		avgV=45
	elif np.mean(gray_img)>30:
		thred=100
		avgV=45
	else:
		thred=10
		avgV=30
	
	kernel = np.ones((3,3),np.uint8)
	kernel = kernel.astype("uint8")

	V_sobel[V_sobel<avgV] = 0
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel_border = V_sobel.copy()
	V_sobel = V_sobel_border[1:height+1, 1:width+1]

	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'5.thred.jpg',V_sobel)
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)

	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)

	small_region = np.mean(small_region)

	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'6.remove_1.jpg',labeled_array1)
	'''
	bg_img, _ = ndimage.measurements.label(bg_img)
	bg_img = cv2.dilate(bg_img.astype(np.float32),kernel,iterations = 1)
	'''
	
	bg_img[bg_img>0]=255
	
	labeled_array1[bg_img == 255]=0
	labeled_array1[labeled_array1>0]=255
	
	cv2.imwrite('seg_result/'+str(name)+'6.remove_2.jpg',labeled_array1)
	
	labeled_array1 = cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations = 2) #dilation 白色區域-膨脹
	labeled_array1 = cv2.erode(labeled_array1.astype(np.float32),kernel,iterations = 1)
	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)

	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	cv2.imwrite('seg_result/'+str(name)+'7.floodfill.jpg',im_out)
	
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)

	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)

	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0

	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.01,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'8.floodfill.jpg',im_out)
	
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=2)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=2)

	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)

	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1)

	im_out[im_out>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'9.remove_small_objects.jpg',im_out)
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)

	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]

	im_out, num_features = ndimage.measurements.label(im_out)
	'''
	test2 = []
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2
		if value<0.03:#0.024
			test2.append(region.label)

	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	'''
	im_out[im_out>0]=255
	#邊界各增加70 避免旋轉後裁切到商品

	im_out = cv2.copyMakeBorder(im_out, 70, 70, 70,70, cv2.BORDER_CONSTANT, value=0)
	if raw_width==960:
		raw_img = cv2.copyMakeBorder(raw_img, 210, 210, 210,210, cv2.BORDER_CONSTANT, value=[0,0,0])
	elif raw_width==1920:
		raw_img = cv2.copyMakeBorder(raw_img, 420, 420, 420,420, cv2.BORDER_CONSTANT, value=[0,0,0])
	else:
		raw_img = cv2.copyMakeBorder(raw_img, 630, 630, 630,630, cv2.BORDER_CONSTANT, value=[0,0,0])

	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []

	(conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	count=0
	if len(conts) > 0:
		print int((init_time-poposal_time).total_seconds())
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			if raw_width==960:
				resize_rect = imgPcsr.rectResize(rect, 3.0)
			elif raw_width==1920:
				resize_rect = imgPcsr.rectResize(rect, 6.0)
			else:
				resize_rect = imgPcsr.rectResize(rect, 9.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			#center[0] = raw_img.shape[1] - center[0]
			#img_crop = np.expand_dims(img_crop, axis=0)#加一個維度
			cropped_images.append(img_crop)
			#把座標貼回原始大圖,並將增加的210減掉
			
			pts[:,0]+=450-630
			pts[:,1]+=150-630
	
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			if int((init_time-poposal_time).total_seconds())>60:
				cv2.imwrite(str(init_time)+'error_seg'+str(count)+'.jpg',img_crop)
				print 'time out'
				poposal_time= datetime.now()
			
		count+=1
	else:
		item_text.append('無商品請重新擺放')
	return cropped_images, bboxes, centroids, rect_data, item_text





