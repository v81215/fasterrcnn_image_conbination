#coding=utf-8
import sys
sys.path.insert(0,'function/')
import cv2
import numpy as np
import string
import csv
import pickle
import string
from datetime import datetime
from keras.models import load_model
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import SGD
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input

from label_image import label_image
from preprocess_image import pad_and_resize, object_proposal_edge
#from preprocess_image_v3 import object_proposal_edge, pad_and_resize 
from folderReader import getfile
from generate_dataset import generate_dataset, load_data
from parallel import make_parallel


def get_category(str):
	cat = int(str.split('_')[0].split('B')[1])-1
	return cat

def predict_dataset(dataset_dir, model_dir):
	print model_dir
	print getfile(model_dir)[-1:][0]
	model = load_model(model_dir+getfile(model_dir)[-1:][0])
	X, Y = load_data(dataset_dir+'testing.h5')

	X = X.astype('float32')
	X -= np.mean(X,axis=0)
	X /= np.std(X,axis=0)

	correct_category = 0
	for i in range(len(X)):
		x = np.expand_dims(X[i],axis=0)
		category = Y[i]
		
		prediction = model.predict(x)
		pred_cat = prediction[0].argmax()

		if pred_cat != category:
			print '--------------------------------------------'
			print 'category:%10s  ,  pred_category:%10s' %(category+1,pred_cat+1)
		else:
			correct_category +=1
	category_acc = float(correct_category)/float(len(X))
	
	print '--------------------------------------------'
	print 'model: %s' %model
	print 'category_accurarcy: %.3f' %category_acc
	print ''


def predict_by_img(image_dir, model_dir):
	#get last model
	model = getfile(model_dir)[-1:][0]
	#get testing image
	imgList = getfile(image_dir)
	#load model
	my_model = load_model(model_dir+model)
	print model
	for img in imgList:
		x = cv2.imread(image_dir+img)
		x = cv2.resize(x,(224,224))
		x = x.astype('float32')
		x /= 255
		x = np.expand_dims(x,axis=0)

		prediction = my_model.predict(x)
		print '---------------------'
		print 'image: %s'%img
		print 'pred_category: %d' %prediction[0].argmax()
	print '---------------------'


def auto_predict(test_data_dir, weight_dir):
	obj_dir = test_data_dir+'testing/object/'
	bgd_dir = test_data_dir+'testing/background/'
	output_img_dir = test_data_dir+'object_proposal/'

	obj_names = getfile(obj_dir)
	bgd_names = getfile(bgd_dir)
	weight = getfile(weight_dir)

	model = load_model(weight_dir+weight[-1:][0])

	for i in range(len(obj_names)):
		print 'reading..  [%s, %s] (%d/%d)' %(obj_names[i],bgd_names[i],i+1,len(obj_names))
		img = cv2.imread(obj_dir+obj_names[i])
		bgd = cv2.imread(bgd_dir+bgd_names[i])
		output_img = img.copy()
		print 'object proposalling... '
		crop_imgs, bboxes = object_proposal_subtract_background(img,bgd)
		print 'padding and resizing... '
		pad_imgs = pad_and_resize(crop_imgs,224)

		for j in range(len(pad_imgs)):
			print 'predicting...'

			img = pad_imgs[j]
			img.astype('float32')
			img /= 255
			img = np.expand_dims(img,axis=0)
			prediction = model.predict(img)
			pred_category = prediction[0].argmax()

			cv2.drawContours(output_img,[bboxes[j]],0,(0,0,255),16)
			cv2.putText(output_img, str(pred_category),
					(bboxes[j][2][0]-100,bboxes[j][2][1]-100), 
					cv2.FONT_HERSHEY_SIMPLEX, 5, 
					(0,0,255), 16, cv2.LINE_AA)
		print output_img_dir+'test'+str(i)+'.jpg'
		cv2.imwrite(output_img_dir+'test'+str(i)+'.jpg',output_img)


def auto_predict_with_precision(test_data_dir, weight_dir, config_path):
	obj_dir = test_data_dir+'tmp/'
	obj_names = getfile(obj_dir)

	weight = getfile(weight_dir)	
	model = load_model(weight_dir+weight[-1:][0])
	
	with open(config_path, 'rb') as f_in:
		C = pickle.load(f_in)

	with open('Items_cat105_test4_summary.csv','wb') as csvfile:
		csvwriter = csv.writer(csvfile, delimiter=',')
		
		for i in range(len(obj_names)):
			correct_category = 0
			proposal_count = 0

			print 'reading..  %s (%d/%d)' %(obj_names[i],i+1,len(obj_names))
			img = cv2.imread(obj_dir+obj_names[i], -1)
			#img = cv2.resize(img, (img.shape[1]/8, img.shape[0]/8))
			#gray = cv2.imread(obj_dir+obj_names[i], 0)
			gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			output_img = img.copy()
			print img.shape, gray.shape

			print 'object proposalling... '
			crop_imgs, bboxes = object_proposal_edge(img, gray, obj_names[i])

			print 'padding and resizing... '
			pad_imgs = pad_and_resize(crop_imgs,224)

			proposal_count += len(pad_imgs)
			for j in range(len(pad_imgs)):
				print 'predicting...'
				
				x = pad_imgs[j]
				x = x.astype('float32')
				x /= 255
				#x -= C.img_channel_mean
				#x /= C.std_scaling
				x = np.expand_dims(x,axis=0)
				prediction = model.predict(x)
				pred_category = prediction[0].argmax()
			
				#category = get_category(obj_names[i])
				
				sort_score = np.sort(prediction[0])
				sort_cat = np.argsort(prediction[0])
				top1_cat = sort_cat[-1]
				top1_score = sort_score[-1]
				top2_cat = sort_cat[-2]
				top2_score = sort_score[-2]
				top3_cat = sort_cat[-3]
				top3_score = sort_score[-3]

				csvwriter.writerow([obj_names[i], str(top1_cat+1), str(top1_score), str(top2_cat+1), str(top2_score), str(top3_cat+1), str(top3_score)])

def auto_predict_for_demo(test_data_dir, weight_dir, config_path):
	obj_dir = test_data_dir+'testing/object2/'
	output_img_dir = test_data_dir+'object_proposal/'
	
	obj_names = getfile(obj_dir)
	weight = getfile(weight_dir)[-1:][0]
	
	print weight_dir+weight
	model = load_model(weight_dir+weight)
	
	with open(config_path, 'rb') as f_in:
		C = pickle.load(f_in)
	
	img = cv2.imread(obj_dir+obj_names[0])
	gray = cv2.imread(obj_dir+obj_names[0], 0)
	crop_imgs, bboxes = object_proposal_edge3(img,gray)

	while True:
		choice = raw_input('Press enter to continue : ')

		obj_names = getfile(obj_dir)

		for i in range(len(obj_names)):
			start_time = datetime.now()
			
			print 'reading..  %s (%d/%d)' %(obj_names[i],i+1,len(obj_names))
			img = cv2.imread(obj_dir+obj_names[i])
			gray = cv2.imread(obj_dir+obj_names[i], 0)
			read_time = datetime.now()
			print 'reading time : {}'.format((read_time-start_time).total_seconds())

			print 'object proposalling... '
			crop_imgs, bboxes = object_proposal_edge3(img,gray)
			proposal_time =  datetime.now()
			print 'propodal time : {}'.format((proposal_time-read_time).total_seconds())
			
			print 'padding and resizing... '
			pad_imgs = pad_and_resize(crop_imgs,224)
			pad_time =  datetime.now()
			print 'padded time : {}'.format((pad_time-proposal_time).total_seconds())
            
			print 'predicting...'
			x = pad_imgs[0]
			x = x.astype('float32')
			x -= C.img_channel_mean
			x /= C.std_scaling
			x = np.expand_dims(x,axis=0)
			prediction = model.predict(x)
			pred_category = prediction[0].argmax()
				
			category = get_category(obj_names[i])
			predict_time = datetime.now()
				
			print '------------------------------------------------'
			print 'image: %s'%obj_names[i]
			print 'predict output: %d' %(pred_category+1)
			print 'predict time : {}'.format((predict_time-pad_time).total_seconds())
			print ''

