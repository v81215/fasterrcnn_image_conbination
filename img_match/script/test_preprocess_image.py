# coding=utf-8
import sys
sys.path.insert(0,'function/')
import cv2
import csv
import math
import numpy as np
import re
import runlength
from scipy import ndimage
from skimage import data,segmentation,measure,morphology,color
from skimage.measure import regionprops
from skimage.filters import threshold_otsu
from skimage.filters.rank import entropy
from skimage.morphology import disk

from folderReader import getfile, getsubdir
import imageProcessor as imgPcsr

imgPath = 'Test_Data/camera/backup/item3/'
#imgNames = getfile(imgPath,isSort=True)

def object_proposal_edge_black_v2(object_img, gray_img, name):
	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape

	object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
	gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))

	otsu_img = object_img.copy()
	height, width = gray_img.shape
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)
	
	V = object_img[:,:,2]
	V = V*255
	V = 255-V
	
	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	V2 = imgPcsr.gamma_equalization(V,2)
	cv2.imwrite(imgPath+name.split('.')[0]+'_1_Gaussian.jpg',V)	

	V_sobel = imgPcsr.sobel(V)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
	cv2.imwrite(imgPath+name.split('.')[0]+'_2_Sobel.jpg',V)		
	
	if np.mean(gray_img)>95:
		thred=500	
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500	
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500	
		avgV=70
	elif np.mean(gray_img)>50:
		thred=40
		avgV=65
	elif np.mean(gray_img)>30:
		thred=30
		avgV=50
	else:
		thred=10
		avgV=30

	V_sobel[V_sobel<avgV] = 0
	cv2.imwrite(imgPath+name.split('.')[0]+'_3_totalsobel.jpg',V_sobel)
	
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	cv2.imwrite(imgPath+name.split('.')[0]+'_4_totalsobel_white.jpg',V_sobel)
	
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)
	
	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)
	
	small_region = np.mean(small_region)
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255
	cv2.imwrite(imgPath+name.split('.')[0]+'_5_remove_small_objects.jpg',labeled_array1)

	kernel = np.ones((3,3),np.uint8)
	kernel = kernel.astype("uint8")
	
	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)
	cv2.imwrite(imgPath+name.split('.')[0]+'_6_dilation.jpg',labeled_array1)

	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	cv2.imwrite(imgPath+name.split('.')[0]+'_7_floodfill.jpg',im_out)	

	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)
	
	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)
	
	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.02,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255
	cv2.imwrite(imgPath+name.split('.')[0]+'_8_removelongarea.jpg',im_out)

	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=2)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=2)

	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)

	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1)

	im_out[im_out>0] = 255
	cv2.imwrite(imgPath+name.split('.')[0]+'_9_erosion.jpg',im_out)

	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)

	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]

	im_out, num_features = ndimage.measurements.label(im_out)
	test2 = []
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2
		if value<0.024:
			test2.append(region.label)
	
	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	
	im_out[im_out>0]=255
	cv2.imwrite(imgPath+name.split('.')[0]+'_10_final.jpg',im_out)

	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []

	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

	if len(conts) > 0:
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			resize_rect = imgPcsr.rectResize(rect, 9.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			center[0] = raw_img.shape[1] - center[0]
			img_crop = np.expand_dims(img_crop, axis=0)
			cropped_images.append(img_crop)
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite(imgPath+name.split('.')[0]+'_11_contour.jpg', raw_img)
	else:
		item_text.append('無商品請重新擺放')

	return cropped_images, bboxes, centroids, rect_data, item_text

def pad_and_resize(img_list,size):
	images = []
	for img in img_list:
		img = np.squeeze(img, axis=0)
		#pad
		pad_img = imgPcsr.pad(img)
		#resize
		output_img = cv2.resize(pad_img,(size,size))
		images.append(output_img)
	return images

