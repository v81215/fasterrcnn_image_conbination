import sys
sys.path.insert(0,'function/')
import csv

from folderReader import getfile
from csvDictConverter import csvDictConverter

def label_image(processed_img_dir, proposal_count_dir, category_label_dir):
	#transfer proposal_count.csv to dict
	dict_proposal = csvDictConverter(proposal_count_dir)
	#all proposals
	object = getfile(processed_img_dir)
	sorted_proposal = []
	labels = {}
	
	#return sorted values by list
	for key in sorted(dict_proposal.iterkeys()):
		sorted_proposal.append(int(dict_proposal[key]))

	index_count = 0
	proposal_count = sorted_proposal[index_count]

	for i in range(len(object)):
		if i >= proposal_count:
			index_count+=1
			proposal_count += sorted_proposal[index_count]		
		labels[object[i]] = index_count

	print 'writing...  [%s]' %category_label_dir
	#transfer dict to csv and save to label_dir
	with open(category_label_dir,'wb') as f:
		w = csv.writer(f)
		w.writerow(labels.keys())
		w.writerow(labels.values())

def label_all_images(source_img_dir, proposal_count_dir, category_label_dir):
	
	
	print 'labelling...  [%s]' %(source_img_dir+'training')
	label_image(source_img_dir+'training/', proposal_count_dir+'training_proposal_count.csv',
			category_label_dir+'training.csv')

	print 'labelling...  [%s]' %(source_img_dir+'validation')
	label_image(source_img_dir+'validation/', proposal_count_dir+'validation_proposal_count.csv',
			category_label_dir+'validation.csv')
