# coding=utf-8
import sys
sys.path.insert(0,'function/')
import cv2
import csv
import math
import numpy as np
import re
import runlength
from scipy import ndimage
from skimage import data,segmentation,measure,morphology,color
from skimage.measure import regionprops
from skimage.filters import threshold_otsu
from skimage.filters.rank import entropy
from skimage.morphology import disk

#from folderReader import getfile, getsubdir
import imageProcessor as imgPcsr

#subtract two input image and find bounding box
#return cropped img and bounding box coordinate
def object_proposal_edge_black(object_img, gray_img, name):
	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	if raw_height==960:
		object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
		gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	elif raw_height==1920:
		object_img = cv2.resize(object_img, (raw_width/6, raw_height/6))
		gray_img = cv2.resize(gray_img, (raw_width/6, raw_height/6))
	else :
		object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
		gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))
	
	otsu_img = object_img.copy()
	height, width = gray_img.shape
	#raw=img.copy()
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)
	
	V = object_img[:,:,2]
	V = V*255
	V = 255-V
	
	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	V2 = imgPcsr.gamma_equalization(V,2)
	
	V_sobel = imgPcsr.sobel(V)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
																					
	if np.mean(gray_img)>75:
		thred=500
		avgV=70
	elif np.mean(gray_img)>30:
		thred=30
		avgV=50
	else:
		thred=10
		avgV=30
	
	V_sobel[V_sobel<avgV] = 0
	
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)
	
	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)
	
	small_region = np.mean(small_region)
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255

	kernel = np.ones((5,5),np.uint8)
	kernel = kernel.astype("uint8")
	labeled_array1 = cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations=2)
	
	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)
	
	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)
	
	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0
		
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.02,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255

	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=3)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=1)
	
	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1)
	
	im_out[im_out>0] = 255

	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]
	
	im_out, num_features = ndimage.measurements.label(im_out)
	test2 = []
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2
		if value<0.024:
			test2.append(region.label)

	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	
	im_out[im_out>0]=255
	'''
	im_out = cv2.copyMakeBorder(im_out, 70, 70, 70,70, cv2.BORDER_CONSTANT, value=0)
	raw_img = cv2.copyMakeBorder(raw_img, 210, 210, 210,210, cv2.BORDER_CONSTANT, value=[0,0,0])
	'''
	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []
	
	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	
	if len(conts) > 0:
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			if raw_height==960:
				resize_rect = imgPcsr.rectResize(rect, 3.0)
			elif raw_height==1920:
				resize_rect = imgPcsr.rectResize(rect, 6.0)
			else:
				resize_rect = imgPcsr.rectResize(rect, 9.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			img_crop = np.expand_dims(img_crop, axis=0)
			cropped_images.append(img_crop)
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite('Test_Data/testing/contour/'+name, raw_img)
	else:
		item_text.append('無商品請重新擺放')
	
	return cropped_images, bboxes, centroids, rect_data, item_text

def object_proposal_edge_black_v2(object_img,bg_img, name):#it's v4 version
	
	
	object_img=object_img[150:870,450:1410]#4:3
	gray_img=object_img.copy()
	gray_img=cv2.cvtColor(gray_img,cv2.COLOR_BGR2GRAY)

	bg_height, bg_width = bg_img.shape
	bg_img = cv2.resize(bg_img, (bg_width/3, bg_height/3))
	cv2.imwrite('Test_Data/testing/bg.jpg',bg_img)
	bg_img[bg_img > 175]=255
	bg_img[bg_img < 175]=0

	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	if raw_width==960:
		print 960
		object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
		gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	elif raw_width==1920:
		object_img = cv2.resize(object_img, (raw_width/6, raw_height/6))
		gray_img = cv2.resize(gray_img, (raw_width/6, raw_height/6))
	else :
		object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
		gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))
	
	otsu_img = object_img.copy()
	height, width = gray_img.shape
	#raw=img.copy()
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)
	
	V = object_img[:,:,2]
	V = V*255
	V = 255-V
	cv2.imwrite('seg_result/'+str(name)+'1.V.jpg',V)
	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	cv2.imwrite('seg_result/'+str(name)+'2.gamma.jpg',V)
	V = np.insert(V, height, 0, axis=0)
	V = np.insert(V, width, 0, axis=1)
	V = np.insert(V, 0, 0, axis=0)
	V = np.insert(V, 0, 0, axis=1)
	V_sobel = imgPcsr.sobel(V)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	cv2.imwrite('seg_result/'+str(name)+'3.sobel.jpg',V_sobel)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
	cv2.imwrite('seg_result/'+str(name)+'4.sobel+gamma.jpg',V_sobel)
	print np.mean(gray_img)
	if np.mean(gray_img)>95:
		thred=500	
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500	
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500	
		avgV=70
	elif np.mean(gray_img)>50:
		thred=40
		avgV=65
	elif np.mean(gray_img)>30:
		thred=30
		avgV=50
	else:
		thred=10
		avgV=30

	V_sobel[V_sobel<avgV] = 0
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'5.thred.jpg',V_sobel)
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)
	
	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)
	
	small_region = np.mean(small_region)
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255
	labeled_array1[bg_img == 255]=0
	labeled_array1[labeled_array1>0]=255

	cv2.imwrite('seg_result/'+str(name)+'6.remove.jpg',labeled_array1)
	kernel = np.ones((3,3),np.uint8)
	kernel = kernel.astype("uint8")
	labeled_array1 = cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations = 2) #dilation 白色區域-膨脹
	labeled_array1 = cv2.erode(labeled_array1.astype(np.float32),kernel,iterations = 2)
	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	cv2.imwrite('seg_result/'+str(name)+'7.floodfill.jpg',im_out)
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)
	
	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)
	
	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.015,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'8.floodfill.jpg',im_out)
	
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=2)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=2)
	
	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1)
	
	im_out[im_out>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'9.remove_small_objects.jpg',im_out)
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]
	
	im_out, num_features = ndimage.measurements.label(im_out)
	test2 = []
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2
		if value<0.024:
			test2.append(region.label)
	
	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	
	im_out[im_out>0]=255
	#邊界各增加70 避免旋轉後裁切到商品
	
	im_out = cv2.copyMakeBorder(im_out, 70, 70, 70,70, cv2.BORDER_CONSTANT, value=0)
	if raw_width==960:
		raw_img = cv2.copyMakeBorder(raw_img, 210, 210, 210,210, cv2.BORDER_CONSTANT, value=[0,0,0])
	elif raw_width==1920:
		raw_img = cv2.copyMakeBorder(raw_img, 420, 420, 420,420, cv2.BORDER_CONSTANT, value=[0,0,0])
	else:
		raw_img = cv2.copyMakeBorder(raw_img, 630, 630, 630,630, cv2.BORDER_CONSTANT, value=[0,0,0])
	
	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []
	
	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	
	if len(conts) > 0:
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			if raw_width==960:
				resize_rect = imgPcsr.rectResize(rect, 3.0)
			elif raw_width==1920:
				resize_rect = imgPcsr.rectResize(rect, 6.0)
			else:
				resize_rect = imgPcsr.rectResize(rect, 9.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			#center[0] = raw_img.shape[1] - center[0]
			#img_crop = np.expand_dims(img_crop, axis=0)#加一個維度
			cropped_images.append(img_crop)
			#把座標貼回原始大圖,並將增加的210減掉
			
			pts[:,0]+=450-210
			pts[:,1]+=150-210
			
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			#cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite('Test_Data/camera/contour/'+name, raw_img)
	else:
		item_text.append('無商品請重新擺放')
	
	return cropped_images, bboxes, centroids, rect_data, item_text



def object_proposal_edge_black_v4(object_img, gray_img, name):
	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	if raw_width==960:
		print 960
		object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
		gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	elif raw_width==1920:
		object_img = cv2.resize(object_img, (raw_width/6, raw_height/6))
		gray_img = cv2.resize(gray_img, (raw_width/6, raw_height/6))
	else :
		object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
		gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))
	otsu_img = object_img.copy()
	height, width = gray_img.shape
	#raw=img.copy()
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)
	
	V = object_img[:,:,2]
	V = V*255
	V = 255-V
	cv2.imwrite('seg_result/'+str(name)+'1.V.jpg',V)
	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	V = np.insert(V, height, 0, axis=0)
	V = np.insert(V, width, 0, axis=1)
	V = np.insert(V, 0, 0, axis=0)
	V = np.insert(V, 0, 0, axis=1)
	V_sobel = imgPcsr.sobel(V)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
	cv2.imwrite('seg_result/'+str(name)+'2.V_sobel.jpg',V_sobel)
	if np.mean(gray_img)>95:
		thred=500	
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500	
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500	
		avgV=70
	elif np.mean(gray_img)>50:
		thred=40
		avgV=65
	elif np.mean(gray_img)>30:
		thred=30
		avgV=50
	else:
		thred=10
		avgV=30
	print avgV
	V_sobel[V_sobel<avgV+10] = 0
	cv2.imwrite('seg_result/'+str(name)+'3.V_sobel_thred.jpg',V_sobel)
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'4.V_sobel_thred.jpg',V_sobel)
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)
	
	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)
	
	small_region = np.mean(small_region)
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'5.remove_smallarea.jpg',labeled_array1)
	kernel = np.ones((3,3),np.uint8)
	kernel = kernel.astype("uint8")
	
	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	cv2.imwrite('seg_result/'+str(name)+'6.floodfill.jpg',im_out)
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)
	cv2.imwrite('seg_result/'+str(name)+'7.floodfill.jpg',im_out)
	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)
	
	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.02,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255
	cv2.imwrite('seg_result/'+str(name)+'8.min_seze.jpg',im_out)
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=2)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=2)
	
	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1)
	
	im_out[im_out>0] = 255
	
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]
	
	im_out, num_features = ndimage.measurements.label(im_out)
	test2 = []
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2
		if value<0.024:
			test2.append(region.label)
	
	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	
	im_out[im_out>0]=255
	
	im_out = cv2.copyMakeBorder(im_out, 120, 120, 120,120, cv2.BORDER_CONSTANT, value=0)
	if raw_width==960:
		raw_img = cv2.copyMakeBorder(raw_img, 360, 360, 360,360, cv2.BORDER_CONSTANT, value=[0,0,0])
	elif raw_width==1920:
		raw_img = cv2.copyMakeBorder(raw_img, 420, 420, 420,420, cv2.BORDER_CONSTANT, value=[0,0,0])
	else:
		raw_img = cv2.copyMakeBorder(raw_img, 630, 630, 630,630, cv2.BORDER_CONSTANT, value=[0,0,0])
	
	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []
	
	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	
	if len(conts) > 0:
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			if raw_width==960:
				resize_rect = imgPcsr.rectResize(rect, 3.0)
			elif raw_width==1920:
				resize_rect = imgPcsr.rectResize(rect, 6.0)
			else:
				resize_rect = imgPcsr.rectResize(rect, 9.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			#center[0] = raw_img.shape[1] - center[0]
			#img_crop = np.expand_dims(img_crop, axis=0)#加一個維度
			cropped_images.append(img_crop)
			#把座標貼回原始大圖,並將增加的210減掉
			pts=pts.astype('float64')
			pts[:,0]-=360
			pts[:,1]-=360
			pts[:,0]*=1.5
			pts[:,1]*=1.5
			pts[pts<0]=0
			pts=pts.astype('int64')
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			#cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite('Test_Data/camera/contour/'+name, raw_img)
	else:
		item_text.append('無商品請重新擺放')
	
	return cropped_images, bboxes, centroids, rect_data, item_text



def object_proposal_edge_black_v3(object_img, gray_img, bg_img, name):
	print 1000
	bg_height, bg_width = bg_img.shape
	bg_img = cv2.resize(bg_img, (bg_width/3, bg_height/3))
	
	cv2.imwrite('Test_Data/testing/bg.jpg',bg_img)
	bg_img[bg_img > 175]=255
	bg_img[bg_img < 175]=0
	kernel = np.ones((3,3),np.uint8) #mask
	kernel=kernel.astype("uint8")
	bg_img = cv2.dilate(bg_img.astype(np.float32),kernel,iterations = 1) #dilation 白色區域-膨脹
	
	
	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	
	
	object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
	gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	
	otsu_img = object_img.copy()
	height, width = gray_img.shape
	#raw=img.copy()
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)
	
	V = object_img[:,:,2]
	V = V*255
	V = 255-V
	
	V_otsu,V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
		
	V_sobel = imgPcsr.sobel(V)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)
	
	if np.mean(gray_img)>105:
		thred=500	
		avgV=100
	elif np.mean(gray_img)>95:
		thred=500	
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500	
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500	
		avgV=70
	elif np.mean(gray_img)>50:
		thred=40
		avgV=65
	elif np.mean(gray_img)>30:
		thred=30
		avgV=50
	else:
		thred=10
		avgV=30

	V_sobel[V_sobel<avgV] = 0
	
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]
	
	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255
	cv2.imwrite('_0_result.jpg',V_sobel)
	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)
	
	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)
	
	small_region = np.mean(small_region)
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	

	labeled_array1[bg_img == 255]=0
	labeled_array1[labeled_array1>0]=255
	cv2.imwrite('_1_result.jpg',labeled_array1)
	
	kernel = np.ones((3,3),np.uint8)
	kernel = kernel.astype("uint8")
	labeled_array1 = cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations = 4) #dilation 白色區域-膨脹

	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	cv2.imwrite('_2_result.jpg',im_out)
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)
	labeled_array1 = cv2.dilate(im_out.astype(np.float32),kernel,iterations = 2) #dilation 白色區域-膨脹
	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)
	
	for i in range (0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0
	
	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.02,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255
	cv2.imwrite('_3_result.jpg',im_out)
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=3)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=1)
	
	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.005,connectivity=1)
	
	im_out[im_out>0] = 255
	cv2.imwrite('_4_result.jpg',im_out)
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	
	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]
	
	im_out, num_features = ndimage.measurements.label(im_out)
	test2 = []
	for region in measure.regionprops(im_out):
		value=region.area/region.perimeter**2
		if value<0.024:
			test2.append(region.label)
	
	for i in range (0,len(test2)):
		im_out[im_out==test2[i]]=0
	
	im_out[im_out>0]=255
	
	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []
	
	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	
	if len(conts) > 0:
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			resize_rect = imgPcsr.rectResize(rect, 3.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			center[0] = raw_img.shape[1] - center[0]
			img_crop = np.expand_dims(img_crop, axis=0)
			cropped_images.append(img_crop)
			
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite('Test_Data/testing/contour/'+name, raw_img)
	else:
		item_text.append('無商品請重新擺放')
	
	return cropped_images, bboxes, centroids, rect_data, item_text


def object_proposal_edge_gray(object_img, gray_img, name):
	raw_img = object_img.copy()
	raw_height, raw_width, _ = object_img.shape
	if raw_height==960:
		object_img = cv2.resize(object_img, (raw_width/3, raw_height/3))
		gray_img = cv2.resize(gray_img, (raw_width/3, raw_height/3))
	elif raw_height==1920:
		object_img = cv2.resize(object_img, (raw_width/6, raw_height/6))
		gray_img = cv2.resize(gray_img, (raw_width/6, raw_height/6))
	else :
		object_img = cv2.resize(object_img, (raw_width/9, raw_height/9))
		gray_img = cv2.resize(gray_img, (raw_width/9, raw_height/9))
	
	height, width = gray_img.shape
	#raw=img.copy()
	otsu_img = object_img.copy()
	
	object_img = cv2.cvtColor(object_img, cv2.COLOR_BGR2HSV)

	R = otsu_img[:,:,0]
	B = otsu_img[:,:,1]
	G = otsu_img[:,:,2]

	V = object_img[:,:,2]
	V = V*255
	V = 255-V

	R_otsu, R_th = cv2.threshold(R,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	B_otsu, B_th = cv2.threshold(B,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	G_otsu, G_th = cv2.threshold(G,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	V_otsu, V_th = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	R = imgPcsr.gamma_equalization3(R,1,R_otsu)
	B = imgPcsr.gamma_equalization3(B,1,B_otsu)
	G = imgPcsr.gamma_equalization3(G,1,G_otsu)
	V = imgPcsr.gamma_equalization3(V,1,V_otsu)
	V2 = imgPcsr.gamma_equalization(V,2)

	#Gaussian blur
	#R = cv2.GaussianBlur(R,(7,7),3)
	#B = cv2.GaussianBlur(B,(7,7),3)
	#G = cv2.GaussianBlur(G,(7,7),3)
	#img2= cv2.GaussianBlur(V,(7,7),3)

	R_sobel = imgPcsr.sobel(R)
	B_sobel = imgPcsr.sobel(B)
	G_sobel = imgPcsr.sobel(G)
	V_sobel = imgPcsr.sobel(V)

	R_sobel = imgPcsr.gamma_equalization(R_sobel,1)
	B_sobel = imgPcsr.gamma_equalization(B_sobel,1)
	G_sobel = imgPcsr.gamma_equalization(G_sobel,1)
	V_sobel = imgPcsr.gamma_equalization(V_sobel,1)

	avgR,th1 = cv2.threshold(R_sobel,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	avgB,th2 = cv2.threshold(B_sobel,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	avgG,th3 = cv2.threshold(G_sobel,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	avgV,th4 = cv2.threshold(V,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	if  np.mean(gray_img)>95:
		thred=300
		avgR=70
		avgB=70
		avgG=70
		avgV=90
	elif np.mean(gray_img)>85:
		thred=500
		avgR=70
		avgB=70
		avgG=70
		avgV=80
	elif np.mean(gray_img)>75:
		thred=500
		avgR=70
		avgB=70
		avgG=70
		avgV=70
	elif np.mean(gray_img)>50:
		thred=40
		avgR=50
		avgB=50
		avgG=50
		avgV=65
	elif np.mean(gray_img)>30:
		thred=30
		avgR=50
		avgB=50
		avgG=50
		avgV=50
	else:
		thred=10
		avgR=30
		avgB=30
		avgG=30
		avgV=30

	R_sobel[R_sobel<avgR] = 0
	B_sobel[B_sobel<avgB] = 0
	G_sobel[G_sobel<avgG] = 0
	V_sobel[V_sobel<avgV] = 0

	#runlength
	V_sobel = np.insert(V_sobel, height, 0, axis=0)
	V_sobel = np.insert(V_sobel, width, 0, axis=1)
	V_sobel = np.insert(V_sobel, 0, 0, axis=0)
	V_sobel = np.insert(V_sobel, 0, 0, axis=1)
	V_sobel = runlength.runlength(V_sobel)
	V_sobel = V_sobel[1:height+1, 1:width+1]

	V_sobel[V_sobel<50] = 0
	V_sobel[V_sobel>0] = 255

	labeled_array1, num_features1 = ndimage.measurements.label(V_sobel)

	small_region = []
	for region in measure.regionprops(labeled_array1):
		small_region.append(region.area)

	small_region = np.mean(small_region)

	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=thred,connectivity=1)
	labeled_array1[labeled_array1>0] = 255

	kernel = np.ones((5,5),np.uint8) #mask
	kernel = kernel.astype("uint8")
	labeled_array1 = cv2.dilate(labeled_array1.astype(np.float32),kernel,iterations=2)

	#floodfill
	labeled_array1 = np.insert(labeled_array1, height, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, width, 0, axis=1)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=0)
	labeled_array1 = np.insert(labeled_array1, 0, 0, axis=1)

	im_out = imgPcsr.floodfill(labeled_array1)
	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=height*width*0.01,connectivity=1)

	labeled_array1, num_features = ndimage.measurements.label(im_out)
	test = []
	for region in measure.regionprops(labeled_array1):
		value = region.area/region.perimeter**2
		if value<0.008:
			test.append(region.label)

	for i in range(0,len(test)):
		labeled_array1[labeled_array1==test[i]] = 0

	labeled_array1 = morphology.remove_small_objects(labeled_array1,min_size=height*width*0.02,connectivity=1)
	im_out, num_features1 = ndimage.measurements.label(labeled_array1)
	im_out[im_out>0] = 255

	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)
	im_out = cv2.erode(im_out.astype(np.float32),kernel,iterations=3)
	im_out = cv2.dilate(im_out.astype(np.float32),kernel,iterations=1)

	im_out = im_out[1:height+1, 1:width+1]
	im_out, num_features = ndimage.measurements.label(im_out)
	im_out = morphology.remove_small_objects(im_out,min_size=1000,connectivity=1)

	im_out[im_out>0] = 255

	#整張圖外圍加一層邊界
	im_out = np.insert(im_out, height, 0, axis=0)
	im_out = np.insert(im_out, width, 0, axis=1)
	im_out = np.insert(im_out, 0, 0, axis=0)
	im_out = np.insert(im_out, 0, 0, axis=1)

	im_out = imgPcsr.floodfill(im_out)
	im_out = im_out[1:height+1, 1:width+1]

	im_out, num_features = ndimage.measurements.label(im_out)
	test2 = []
	for region in measure.regionprops(im_out):
		value = region.area/region.perimeter**2
		if value<0.023:
			test2.append(region.label)

	for i in range(0,len(test2)):
		im_out[im_out==test2[i]] = 0

	im_out[im_out>0] = 255

	#邊界各增加70 避免旋轉後裁切到商品
	'''
	im_out = cv2.copyMakeBorder(im_out, 70, 70, 70,70, cv2.BORDER_CONSTANT, value=0)
	if raw_height==960:
		raw_img = cv2.copyMakeBorder(raw_img, 210, 210, 210,210, cv2.BORDER_CONSTANT, value=[0,0,0])
	elif raw_height==1920:
		raw_img = cv2.copyMakeBorder(raw_img, 420, 420, 420,420, cv2.BORDER_CONSTANT, value=[0,0,0])
	else:
		raw_img = cv2.copyMakeBorder(raw_img, 630, 630, 630,630, cv2.BORDER_CONSTANT, value=[0,0,0])
	'''
	cropped_images = []
	bboxes = []
	centroids = []
	rect_data = []
	item_text = []
	
	(_,conts,_) = cv2.findContours(im_out.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

	if len(conts) > 0:
		for cnt in conts:
			rect = cv2.minAreaRect(cnt)
			if raw_height==960:
				resize_rect = imgPcsr.rectResize(rect, 3.0)
			elif raw_height==1920:
				resize_rect = imgPcsr.rectResize(rect, 6.0)
			else:
				resize_rect = imgPcsr.rectResize(rect, 9.0)
			img_crop, pts = imgPcsr.crop_minAreaRect(raw_img, resize_rect)
			pts = np.int0(pts)
			center=(pts[0]+pts[1]+pts[2]+pts[3])/4
			center[0] = raw_img.shape[1] - center[0]
			img_crop = np.expand_dims(img_crop, axis=0)
			cropped_images.append(img_crop)
			bboxes.append(pts)
			centroids.append(center)
			rect_data.append(resize_rect)
			cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite('Test_Data/testing/contour/'+name, raw_img)
	else:
		item_text.append('無商品請重新擺放')
		'''
		img_crop = np.expand_dims(raw_img, axis=0)
		if raw_height==960:
			pts = [[0,0], [width*3,0], [width*3,height*3], [0,height*3]]
		elif raw_height==1920:
			pts = [[0,0], [width*6,0], [width*6,height*6], [0,height*6]]
		else:
			pts = [[0,0], [width*9,0], [width*9,height*9], [0,height*9]]
		pts = np.int0(pts)
		center=(pts[0]+pts[1]+pts[2]+pts[3])/4
		resize_rect = ((0,0), (raw_img.shape[1],raw_img.shape[0]), 0.0)
		cropped_images.append(img_crop)
		bboxes.append(pts)
		centroids.append(center)
		rect_data.append(resize_rect)
		cv2.drawContours(raw_img, [pts], -1, (0, 0, 255), 16)
		cv2.imwrite('Test_Data/testing/contour/'+name, raw_img)
		'''
	#print len(cropped_images)
	return cropped_images, bboxes, centroids, rect_data, item_text

#pad source imgs into square img and resize it
def pad_and_resize(img_list,size):
	images = []
	for img in img_list:
		img = np.squeeze(img, axis=0)
		#pad
		pad_img = imgPcsr.pad(img)
		#resize
		output_img = cv2.resize(pad_img,(size,size))
		images.append(output_img)
	return images

#give object img and background img's folder path, generate cropped image and proposal_count.csv
def process_image(obj_dir, bgd_dir, processed_img_dir, output_image_size, proposal_count_dir):
	obj_names = getfile(obj_dir)
	#bgd_names = getfile(bgd_dir)

	f = open(proposal_count_dir+processed_img_dir.split('/')[-3]+'_proposal_count.csv','wb')
	w = csv.writer(f)
	proposal_count = {}

	for i in range(len(obj_names)):
		print 'reading... [%s, %s] (%d/%d)' %(obj_names[i],bgd_names[i],i+1,len(obj_names))
		#read images
		img = cv2.imread(obj_dir+obj_names[i])
		gray = cv2.imread(obj_dir+obj_names[i],0)
		#bgd = cv2.imread(bgd_dir+bgd_names[i])
		
		#generating object proposal and return crop images and bboxes
		#crop_imgs, bboxes = object_proposal_subtract_background(img,bgd)
		crop_imgs, bboxes = object_proposal_edge2(img, gray)
		
		#pad and resize the cropped image
		output_imgs = pad_and_resize(crop_imgs,output_image_size)
		
		#add proposal count to dict
		proposal_count[obj_names[i]] = len(output_imgs)

		for j in range(len(output_imgs)):
			#save the processed image
			name = processed_img_dir + obj_names[i].split('.')[0] + '_' + str(j) + '.JPG'
			cv2.imwrite(name, output_imgs[j])

		#save original image with contours
		#cv2.imwrite(processed_img_dir+'contours/'+obj_names[i], contours_img)
		
	w.writerow(proposal_count.keys())
	w.writerow(proposal_count.values())
	f.close()


def process_all_images(input_img_dir, processed_img_dir, img_size, proposal_count_dir):
	dir = ['training/','validation/']
	count = 1
	for folder in dir:
		path = input_img_dir+folder
		print 'processing...  [%s] (%d/%d)' %(path, count, len(dir))
		process_image(path+'object/',path+'background/',processed_img_dir+folder,img_size,proposal_count_dir)
		count += 1



