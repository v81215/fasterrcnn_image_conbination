import cv2
import numpy as np
from math import *
import os
class Segmentor:

    def crop_minAreaRect(self, img, rect):
        
        angle = rect[2]
        if angle < -45:
            angle += 90
        
        rows,cols = img.shape[0], img.shape[1]
        height, width=img.shape[:2]
        heightNew=int(width*fabs(sin(radians(angle)))+height*fabs(cos(radians(angle))))
        widthNew=int(height*fabs(sin(radians(angle)))+width*fabs(cos(radians(angle))))

        M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
        M[0,2] +=(widthNew-width)/2  #重点在这步，目前不懂为什么加这步
        M[1,2] +=(heightNew-height)/2  #重点在这步

        #img_rot = cv2.warpAffine(img,M,(cols,rows))
        img_rot = cv2.warpAffine(img,M,(widthNew,heightNew))

        # rotate bounding box
        box = cv2.boxPoints(rect)
        pts = np.int0(cv2.transform(np.array([box]), M))[0]    
        pts[pts < 0] = 0
        '''
        rows,cols = img.shape[0], img.shape[1]
        M = cv2.getRotationMatrix2D((cols/2,rows/2), 0, 1)
        box = cv2.boxPoints(rect)
        print (box)
        pts = np.int0(cv2.transform(np.array([box]), M))[0]   
        print(pts)
        '''
        # crop
        img_crop = img_rot[min(pts[:,1]):max(pts[:,1]), min(pts[:,0]):max(pts[:,0])] #從原圖裁出目標區域img_crop
        coordinate = [[min(pts[:,0]),min(pts[:,1])],[max(pts[:,0]),min(pts[:,1])],[min(pts[:,0]),max(pts[:,1])],[max(pts[:,0]),max(pts[:,1])]]
        return img_crop, coordinate

    def crop_rect(self, x, y, w, h, img):
        rect = img[y:y+h, x:x+w]
        return rect
    def crop_img(self, img, mask):
        
        mask[mask<128] = 0
        mask[mask>=128] = 255
        #img[mask<128] = 0
        (_,conts,_) = cv2.findContours(mask.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in conts:
            x, y, w, h = cv2.boundingRect(cnt)
            img_crop = self.crop_rect(x, y, w, h, img)
            mask_crop = self.crop_rect(x, y, w, h, mask)
            coordinate = [[x, y], [x+w, y+h]]
            # rect = cv2.minAreaRect(cnt)
            # img_crop, coordinate = self.crop_minAreaRect(img, rect)
            # mask_crop, coordinate = self.crop_minAreaRect(mask, rect)
        
        return img_crop, mask_crop, coordinate

if __name__ == '__main__':
    segmentor = Segmentor()
    path = 'result/8product_30imgs/aug_img/'
    folder = os.listdir(path)
    for file in folder:
        img = cv2.imread(path + file)
        mask = cv2.imread('result/8product_30imgs/aug_mask/' + file,0)
        
        img, mask ,coordinate = segmentor.crop_img(img, mask)
        cv2.imwrite('result/8product_30imgs/test_img_crop/' + file, img)
        cv2.imwrite('result/8product_30imgs/test_mask_crop/' + file.split('.')[0] + '_mask.jpg', mask)