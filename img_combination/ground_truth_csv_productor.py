#!/usr/bin/python
# -*- coding: <encoding name> -*-
# coding=gbk
import numpy as np 
import cv2
import os
from segmentor import Segmentor
from image_productor import ImageProductor
import csv
if __name__ == '__main__':
    segmentor = Segmentor()
    f = open("frcnn_groundTruth.csv", "w", newline='')
    w = csv.writer(f)
    path = 'img/'
    folder_list = os.listdir(path)
    folder_list.sort()
    for folder in folder_list:          
        file_list = os.listdir(path + folder)
        for file in file_list:
            if file.find('mask')==-1:
                img = cv2.imread(path + folder + '/' + file)
                #print ('load ' +file)
                mask = cv2.imread(path + folder + '/' + file.split('.')[0]+'_mask.jpg', 0)
                img_crop, mask_crop ,coordinate =  segmentor.crop_img(img, mask)
                #print (img_crop)
                data=[file,coordinate,int(folder)]
                print (data)
                w.writerow(data)
    print ('load success!')