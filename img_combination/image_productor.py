#!/usr/bin/python
# -*- coding: <encoding name> -*-
# coding=gbk
import numpy as np 
import cv2
import random
import os
from segmentor import Segmentor

class ImageClass:

    def __init__(self, class_id):
        self._class_id = class_id
        self._select_num = 0 
        self.img = []
        self.mask = []
        #self.cam_num = []
        self.coordinate_list = []
        self.random_list = []
    @property
    def class_id(self):
        return self._class_id
    
    @property
    def select_num(self):
        return self._select_num
    
    def next_img(self):
        self._select_num+=1
        if len(self.img) == self._select_num:
            self._select_num = 0
    
    def get_random_list(self):
        img_num_list = []
        for index in range(len(self.img)):
            img_num_list.append(index)
        self.random_list = random.sample(img_num_list, len(img_num_list))

    def random_next_img(self):
        if len(self.random_list)==0:
            self.get_random_list()
        ran_num = self.random_list[0]
        self.random_list.remove(ran_num)
        self._select_num = ran_num

class ImageProductor:

    def __init__(self, img_path, mask_path):
        self.imageclass_list = []
        self.random_list = []
        self.load_img(img_path, mask_path)
        
    def load_img(self, img_path, mask_path):

        folder_list = os.listdir(img_path)
        folder_list.sort()
        for folder in folder_list:
            imageclass = ImageClass(int(folder))           
            file_list = os.listdir(img_path + folder)
            file_list.sort()
            for file in file_list:
                    img = cv2.imread(img_path + folder + '/' + file)
                    mask = cv2.imread(mask_path + folder + '/' + file, 0)
                    
                    kernel = np.ones((3,3),np.uint8)
                    mask = cv2.erode(mask.astype(np.float32), kernel, iterations = 3)
                    mask = mask.astype('uint8')
                    coordinate =  self.get_coordinate(mask)
                    #print (img_crop)
                    #0~6做長寬比判斷，減少寬度
                    '''
                    if int(file.split('_')[0]) <= 6 and float(img_crop.shape[1]/img_crop.shape[0])<2.3:
                        if (int(file.split('_')[1].split('.')[0])-1) % 4 < 2:
                            img_crop = img_crop[0:int(img_crop.shape[1]/2.3), 0:img_crop.shape[1]]
                            mask_crop = mask_crop[0:int(img_crop.shape[1]/2.3), 0:img_crop.shape[1]]
                        elif (int(file.split('_')[1].split('.')[0])-1) % 4 > 1:
                            img_crop = img_crop[img_crop.shape[0]-int(img_crop.shape[1]/2.3):img_crop.shape[0], 0:img_crop.shape[1]]
                            mask_crop = mask_crop[img_crop.shape[0]-int(img_crop.shape[1]/2.3):img_crop.shape[0], 0:img_crop.shape[1]]
                    #7做長寬比判斷，減少寬度
                    if int(file.split('_')[0]) == 7 and float(img_crop.shape[1]/img_crop.shape[0])<2.8:                        
                        if (int(file.split('_')[1].split('.')[0])-1) % 5 < 4:
                            img_crop = img_crop[0:int(img_crop.shape[1]/2.8), 0:img_crop.shape[1]]
                            mask_crop = mask_crop[0:int(img_crop.shape[1]/2.8), 0:img_crop.shape[1]]
                        elif (int(file.split('_')[1].split('.')[0])-1) % 5 > 3:
                            img_crop = img_crop[img_crop.shape[0]-int(img_crop.shape[1]/2.8):img_crop.shape[0], 0:img_crop.shape[1]]
                            mask_crop = mask_crop[img_crop.shape[0]-int(img_crop.shape[1]/2.8):img_crop.shape[0], 0:img_crop.shape[1]]
                    '''
                    imageclass.mask.append(mask)
                    imageclass.img.append(img)
                   # imageclass.cam_num.append(file.split('_')[2] + '_' + file.split('_')[3])
                    imageclass.coordinate_list.append(coordinate)
                    
            self.imageclass_list.append(imageclass)
        print ('load success!')
    #紀錄商品原始座標，合成時挑最接近的商品座標來貼入背景圖
    def get_coordinate(self, mask):
        mask[mask<128] = 0
        mask[mask>=128] = 255
        #img[mask<128] = 0
        (_,conts,_) = cv2.findContours(mask.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in conts:
            x, y, w, h = cv2.boundingRect(cnt)
            #coordinate = [[y, x], [y, x+w], [y+h, x], [y+h, x+w]]
            coordinate = [y+int(h/2), x+int(w/2)]
        return coordinate
    def get_class(self):
        class_list = []
        for imageclass in self.imageclass_list:
            class_list.append(imageclass.class_id)
        self.random_list = random.sample(class_list, len(class_list))

    def random_select(self):
        if len(self.random_list)==0:
            self.get_class()
        ran_num = self.random_list[0]
        self.random_list.remove(ran_num)
        return ran_num

    def get_img(self, decided_class_num = -1):
        if decided_class_num == -1:
            num = self.random_select()
        else:
            num = decided_class_num
        new_img = self.imageclass_list[num].img[self.imageclass_list[num].select_num]
        new_mask = self.imageclass_list[num].mask[self.imageclass_list[num].select_num]
        new_coordinate = self.imageclass_list[num].coordinate_list[self.imageclass_list[num].select_num]
        img_list_num = self.imageclass_list[num].select_num
        
        self.imageclass_list[num].next_img()
        
        return new_img, new_mask, new_coordinate, num, img_list_num
    
    def get_all_img(self):
        return self.imageclass_list

        
if __name__ == '__main__':
    imageproductor = ImageProductor('raw/20190730_rigid_product/single_img/')
    imageclass_list = imageproductor.get_all_img()
    # for imageclass in imageclass_list:
    #     for x in range(len(imageclass.img)):
    #         cv2.imwrite(str(imageclass.class_id)+'_'+str(x+1)+'.jpg',imageclass.img[x])
    #         cv2.imwrite(str(imageclass.class_id)+'_'+str(x+1)+'_mask.jpg',imageclass.mask[x])
    # cv2.imwrite('mask.jpg',imageclass_list[0].mask[0])
    print (imageclass_list[0].img[0].shape[0])
    img =  imageclass_list[0].img[0].copy()
    img = img[0:img.shape[0],0:img.shape[1]]
    print (img.shape)
    print(len(imageclass_list))