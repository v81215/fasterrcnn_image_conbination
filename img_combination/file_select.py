import cv2
import os


        
if __name__ == '__main__':
    path = 'raw/20190730_rigid_product/img/'
    folder = os.listdir(path)
    folder.sort()
    for file in folder:
        if file.split('_')[3].split('.')[0] == '0':
            img = cv2.imread(path + file)
            mask = cv2.imread('raw/20190730_rigid_product/mask/'+file)
            cv2.imwrite('raw/20190730_rigid_product/single_img/'+file, img)
            cv2.imwrite('raw/20190730_rigid_product/single_mask/'+file, mask)