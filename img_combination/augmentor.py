import cv2
import numpy as np
import random
import os
class Augmentor:

    def change_shift(self, img, down_shift_rate, right_left_shift):
        img = img[0:int(img.shape[0]*(1-down_shift_rate)), 0:img.shape[1]]
        if right_left_shift:
            img = img[0:img.shape[0], int(right_left_shift * img.shape[1]):int(img.shape[1]*(1 - right_left_shift))]
        return img


    def rotate_bound(self, img, angle):
        (h, w) = img.shape[:2]
        (cX, cY) = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
        cos = np.abs(M[0, 0])
        sin = np.abs(M[0, 1])
        nW = int((h * sin) + (w * cos))
        nH = int((h * cos) + (w * sin))
        M[0, 2] += (nW / 2) - cX
        M[1, 2] += (nH / 2) - cY
        rot = cv2.warpAffine(img, M, (nW, nH))
        return rot

    def flip_180(self, img, is_flip):
        flip_img = img.copy()
        if is_flip:
            flip_img = np.rot90(flip_img, 2)
        return flip_img
    def change_lighten(self, img, gamma):
        img = np.uint8(np.clip((img*gamma),0,255))
        return img

    def augmentation(self, img, mask, is_circle = False, is_flip = False,
                    down_shift_rate = 0, right_left_shift_rate = 0,
                    random_angle = 0, gamma_up = 1, gamma_down = 1):
        if down_shift_rate != 0:
            down_shift_rate = random.uniform(0, down_shift_rate)
        if right_left_shift_rate !=0:
            right_left_shift = random.uniform(0, right_left_shift_rate)
        
        if is_circle:#圓形商品作旋轉不做shift
            angle = random.randrange(0, 360, 5)
            right_left_shift = 0
        if is_flip:
            is_flip = random.randint(0, 1)
        
        angle = random.randint(-random_angle, random_angle)

        gamma = random.uniform(gamma_down, gamma_up)

        img = self.change_shift(img, down_shift_rate, right_left_shift)        
        img = self.change_lighten(img, gamma)
        img = self.rotate_bound(img, angle)
        img = self.flip_180(img, is_flip)

        mask = self.change_shift(mask, down_shift_rate, right_left_shift)
        mask = self.rotate_bound(mask, angle)
        mask = self.flip_180(mask, is_flip)
        
        return img, mask


if __name__ == '__main__':
    augmentor = Augmentor()
    path = 'raw/20190705_product/'
    folder = os.listdir(path)
    sample = 4
    for x in range(sample):
        for file in folder:
            img = cv2.imread(path + file)
            mask = cv2.imread('raw/20190705_mask/' + file, 0)
            img, mask = augmentor.augmentation(img, mask, is_circle = False, is_flip = False,
                                                down_shift_rate = 0.001, right_left_shift_rate = 0.001,
                                                random_angle = 15, gamma_up = 1.2, gamma_down = 0.9)
            # if file.split('_')[2] == '1':
            #     img, mask = augmentor.augmentation(img, mask, False, 0.5)
            # if file.split('_')[2] == '2':
            #     img, mask = augmentor.augmentation(img, mask, False, 0.4)
            # else:
            #     img, mask = augmentor.augmentation(img, mask, False, 0.2)
            cv2.imwrite('result/8product_30imgs/aug_mask/' + file.split('.')[0] + '_' + str(x) + '.jpg', mask)
            cv2.imwrite('result/8product_30imgs/aug_img/' + file.split('.')[0] + '_' + str(x) + '.jpg', img)