#!/usr/bin/python
# -*- coding: <encoding name> -*-
# coding=gbk
import numpy as np 
import cv2
import os
path = 'raw/img2/'
folder_list = os.listdir(path)
folder_list.sort()
for folder in folder_list:          
    file_list = os.listdir(path + folder)
    file_list.sort()
    for file in file_list:
        if file.find('mask')==-1:
            img = cv2.imread(path + folder + '/' + file)
            mask = cv2.imread(path + folder + '/' + file.split('.')[0]+'_mask.jpg')
            if int(file.split('_')[1].split('.')[0]) < 10:
                cv2.imwrite(path + folder + '/' + file.split('_')[0]+'_0'+file.split('_')[1], img)
                cv2.imwrite(path + folder + '/' + file.split('_')[0]+'_0'+file.split('_')[1].split('.')[0]+'_mask.jpg', mask)
                os.remove(path + folder + '/' + file)
                os.remove(path + folder + '/' + file.split('.')[0]+'_mask.jpg')