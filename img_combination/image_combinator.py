import cv2
from image_productor import ImageProductor
from augmentor import Augmentor
from segmentor import Segmentor
import csv
import random
import math
import numpy as np
import operator


class ProductImage:
	def __init__(self, img, mask, coordinate):
		self._product_class = 0
		self._product_img = img
		self._product_mask = mask
		self._con_bg_coordinate = coordinate
		self._distance = 0
		self.start_y = 0
		self.start_x = 0
		self.end_y = 0
		self.end_x = 0
		self.get_xy()

	@property
	def product_img(self):
		return self._product_img

	@property
	def product_mask(self):
		return self._product_mask

	@property
	def con_bg_coordinate(self):
		return self._con_bg_coordinate

	@property
	def distance(self):
		return self._distance

	@property
	def product_class(self):
		return self._product_class

	def get_xy(self):
		self.start_y = self._con_bg_coordinate[0] - int(self._product_img.shape[0]/2)
		self.end_y = self._con_bg_coordinate[0] + int(self._product_img.shape[0]/2)
		self.start_x = self._con_bg_coordinate[1] - int(self._product_img.shape[1]/2)
		self.end_x = self._con_bg_coordinate[1] + int(self._product_img.shape[1]/2)

	@distance.setter
	def distance(self, distance):
		self._distance = distance

	@product_class.setter
	def product_class(self, product_class):
		self._product_class = product_class

class ImageConbinator:

	def __init__(self, img_path, mask_path):
		# self.start_coordiante = (84, 503)
		# self.end_coordiante = (507, 914)
		self.start_coordiante = (64, 450)
		self.end_coordiante = (507, 1004)
		self.bg_center = (400, 640)
		self.conbinate_coordinate = [self.start_coordiante[0], self.start_coordiante[1]]


		self.max_lenth = 0
		self.width_overlap = 0.1
		self.lenth_overlap = 0.1
		self.imageproductor = ImageProductor(img_path, mask_path)
		self.augmentor = Augmentor()
		self.segmentor = Segmentor()
		self.imageclass_list = self.imageproductor.get_all_img()

	def select_class_img(self):
		class_num = random.randint(0, len(self.imageclass_list)-1)
		# class_num = 0
		return class_num

	def distance(self, p0, p1):

		return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

	def select_short_distance_product(self, class_num, bg_coordinate):
		distance_list = []
		for coordinate in self.imageclass_list[class_num].coordinate_list:
			# print('class_num:', class_num, coordinate)
			# print('bg:',bg_coordinate)
			distance_list.append(self.distance(coordinate, bg_coordinate))
		distance_list = np.array(distance_list)
		# print(np.argmin(distance_list))
		return np.argmin(distance_list)

	# def select_bg_long_distance(self, conbinate_coordiante_list, bg_center):
	# 	distance_list = []
	# 	for coordinate in conbinate_coordiante_list:
	# 		distance_list.append(self.distance(coordinate, bg_center))
	# 	distance_list = np.array(distance_list)
	# 	return  np.argmax(distance_list)

	def image_conbinate(self, bg_img, product_img, product_mask, conbinate_coordinate):
		# print (bg_img.shape, product_img.shape, product_mask.shape, conbinate_coordinate )
		# if conbinate_coordinate[0] - int(product_img.shape[0]/2) < 0:
		# 	product_img = product_img[abs(conbinate_coordinate[0] - int(product_img.shape[0]/2)):product_img.shape[0], 0:product_img.shape[1]]

		# if conbinate_coordinate[1] - int(product_img.shape[1]/2) < 0:
		# 	product_img = product_img[0:product_img.shape[0], abs(conbinate_coordinate[0] - int(product_img.shape[1]/2)):product_img.shape[1]]

		# if conbinate_coordinate[0] + int(product_img.shape[0]/2) > bg_img.shape[0]:
		# 	product_img = product_img[0:bg_img.shape[0]-conbinate_coordinate[0],  0:product_img.shape[1]]

		# if conbinate_coordinate[1] + int(product_img.shape[1]/2) > bg_img.shape[1]:
		# 	product_img = product_img[0:product_img.shape[0],  0:bg_img.shape[1]-conbinate_coordinate[1]]
		
		roi = bg_img[conbinate_coordinate[0] - int(product_img.shape[0]/2) : conbinate_coordinate[0] + int(product_img.shape[0]/2),	
					conbinate_coordinate[1] - int(product_img.shape[1]/2) : conbinate_coordinate[1] + int(product_img.shape[1]/2)]

		product_img[product_mask == 0] = 0		
		roi[product_mask > 0] = 0	
		dst = cv2.add(roi, product_img)
		bg_img[conbinate_coordinate[0] - int(product_img.shape[0]/2) : conbinate_coordinate[0] + int(product_img.shape[0]/2),	
					conbinate_coordinate[1] - int(product_img.shape[1]/2) : conbinate_coordinate[1] + int(product_img.shape[1]/2)] = dst
		return bg_img
	def image_process(self, img, mask):#處理圖形不是偶數問題
		if img.shape[0]%2 == 1:
			img = img[0:img.shape[0]-1, 0:img.shape[1]]
			mask = mask[0:mask.shape[0]-1, 0:mask.shape[1]]
		if img.shape[1]%2 == 1:
			img = img[0:img.shape[0], 0:img.shape[1]-1]
			mask = mask[0:mask.shape[0], 0:mask.shape[1]-1]
		return img, mask
	def select_conbination(self, class_select = -1):
		self.conbinate_coordinate = [self.start_coordiante[0], self.start_coordiante[1]]
		product_list = []
		max_x_shape = 0
		last_y_shape = 0 
		while 1:
			if class_select == -1:
				class_num = self.select_class_img()#隨機抽一類商品
			else:
				class_num = class_select
			product_num = self.select_short_distance_product(class_num, [self.conbinate_coordinate[0],self.conbinate_coordinate[1]])#決定這類商品要拿第幾個商品#不能直接傳list 會只有傳址
			img = self.imageclass_list[class_num].img[product_num].copy()
			mask = self.imageclass_list[class_num].mask[product_num].copy()
			img, mask = self.augmentor.augmentation(img, mask, is_circle = False, is_flip = False,
																down_shift_rate = 0.001, right_left_shift_rate = 0.001,
																random_angle = 5, gamma_up = 1.2, gamma_down = 0.9)			
			img, mask ,coordinate = self.segmentor.crop_img(img, mask)
			img, mask = self.image_process(img, mask)
			
			if self.conbinate_coordinate[0]+int(img.shape[0]/2) < self.end_coordiante[0]:
				overlaprate = 1 - abs(self.bg_center[0] - self.conbinate_coordinate[0])/360.0#把中間分成幾等分，每等分0.1
				overlaprate += 0.2
				if overlaprate > 1:
					overlaprate = 1
				self.conbinate_coordinate[0] = self.conbinate_coordinate[0] + int(img.shape[0] * overlaprate)
				# self.conbinate_coordinate[0] = self.conbinate_coordinate[0] + int((img.shape[0]/2 + last_y_shape/2) * overlaprate)
				productimage = ProductImage(img, mask, [self.conbinate_coordinate[0],self.conbinate_coordinate[1]])#不能直接傳list 會只有傳址
				productimage.distance = self.distance(self.bg_center, productimage.con_bg_coordinate)
				productimage.product_class = class_num
				product_list.append(productimage)
				if img.shape[1] > max_x_shape:
					max_x_shape = img.shape[1]
				last_y_shape = img.shape[0]
				#print('x:',overlaprate, self.conbinate_coordinate[0])
			elif self.conbinate_coordinate[1]+int(img.shape[1]/2) < self.end_coordiante[1]:
				
				overlaprate = 1 - abs(self.bg_center[1] - (self.conbinate_coordinate[1] + img.shape[1]))/640.0
				overlaprate = overlaprate ** 0.2
				overlaprate -= 0.1
				if overlaprate > 1:
					overlaprate = 1
					
				print('y:', overlaprate, self.bg_center[1], self.conbinate_coordinate[1])
				self.conbinate_coordinate[0] = self.start_coordiante[0]
				self.conbinate_coordinate[1] += int(max_x_shape * 0.9 * overlaprate)
				# print('y:', overlaprate, self.bg_center[1], self.conbinate_coordinate[1])
				last_y_shape = 0 
			else:				
				return product_list
		
		
			# 		roi = bg_img[self.width:self.width + img.shape[0], self.lenth:self.lenth + img.shape[1]]
			# 		img[mask == 0] = 0		
			# 		roi[mask > 0] = 0	
			# 		dst = cv2.add(roi,img)
			# 		bg_img[self.width:self.width + img.shape[0], self.lenth:self.lenth+img.shape[1]] = dst
			# 		pts = [
			# 				[self.lenth, self.width],
			# 				[self.lenth, self.width+img.shape[0] - int(self.width_overlap * img.shape[0])],
			# 				[self.lenth+img.shape[1] - int(self.lenth_overlap * img.shape[1]), self.width],
			# 				[self.lenth+img.shape[1] - int(self.lenth_overlap * img.shape[1]), self.width+img.shape[0]- int(self.width_overlap * img.shape[0])]
			# 			]
			# 		data = [pts , str(class_num), str(img_num)]
			# 		csv_list.append(data)
			# 		self.width = self.width + img.shape[0] - int(self.width_overlap * img.shape[0])
			# 		if img.shape[1] > self.max_lenth:
			# 			self.max_lenth = img.shape[1]
					
			# elif self.width + img.shape[0] > bg_img.shape[0]-100:
			# 	self.lenth = self.lenth + self.max_lenth - int(self.lenth_overlap * img.shape[1])
			# 	self.width = 75
			# elif self.lenth + img.shape[1] > bg_img.shape[1]-300:
	# 		# 	lenth_check=False



	def conbinate_sort(self, product_list):
		sorted_product_list = sorted(product_list, key=operator.attrgetter('distance'), reverse=True)
		return sorted_product_list

	def coordinate_finetune(self,product_num, product_list):
		
		mask = np.zeros((720, 1280), np.uint8)
		mask[product_list[product_num].start_y:product_list[product_num].end_y, product_list[product_num].start_x:product_list[product_num].end_x] = 255
		for x in range(product_num+1, len(product_list)):#把會蓋過這個商品的全部再蓋一次
			mask[product_list[x].start_y:product_list[x].end_y, product_list[x].start_x:product_list[x].end_x] = 0
		kernel = np.ones((5,5),np.uint8)
		mask = cv2.erode(mask.astype(np.float32), kernel, iterations = 4)
		mask = cv2.dilate(mask.astype(np.float32), kernel, iterations = 4)
		mask = mask.astype('uint8')
		cv2.imwrite(str(product_num)+'_mask.jpg', mask)
		(_,conts,_) = cv2.findContours(mask.astype(np.uint8),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
		for cnt in conts:
			x, y, w, h = cv2.boundingRect(cnt)
			product_list[product_num].start_y = y
			product_list[product_num].start_x = x
			product_list[product_num].end_y = y+h
			product_list[product_num].end_x = x+w
	def random_remove(self, sorted_product_list, remove_rate):
		times = int(len(sorted_product_list)*remove_rate)
		for x in range(times):
			# print(len(sorted_product_list))
			index =  random.randint(0, len(sorted_product_list))
			
			# print(index)
			del sorted_product_list[index-1]
	def reset_parameter(self):
		self.width_overlap = random.uniform(0, 0.3)
		#self.width_overlap = 0.2
		self.lenth_overlap = random.uniform(0, 0.3)
		#self.lenth_overlap = 0.2
		self.width = 75
		self.lenth = 175
		self.max_lenth = 0

if __name__ == '__main__':
	imageconbinator = ImageConbinator('raw/20190705_product/', 'raw/20190705_mask/' )
	
	f = open("result/no_ridgid_product_20190806/frcnn_1500+500.csv", "w", newline='')
	w = csv.writer(f)
	for imageclass in imageconbinator.imageclass_list:
		for x in range(len(imageclass.img)):
			img = imageclass.img[x].copy()
			mask = imageclass.mask[x].copy()
			_, _ , coordinate = imageconbinator.segmentor.crop_img(img, mask)
			#cv2.rectangle(img, (coordinate[0][0], coordinate[0][1]), (coordinate[1][0], coordinate[1][1]), (0, 255, 0), 2)
			data = ['single_'+str(imageclass.class_id)+'_'+str(x)+'.jpg', coordinate, imageclass.class_id]
			w.writerow(data)
			cv2.imwrite('result/ridgid_proudct_20190801/train/single_'+str(imageclass.class_id)+'_'+str(x)+'.jpg', img)
			print('single_'+str(imageclass.class_id)+'_'+str(x)+'.jpg' + 'finished!')
	for x in range(2000):
		if x < len(imageconbinator.imageclass_list)*10:
			product_list = imageconbinator.select_conbination(int(x/10))
		else:
			product_list = imageconbinator.select_conbination()
		sorted_product_list = imageconbinator.conbinate_sort(product_list)
		imageconbinator.random_remove(sorted_product_list, random.uniform(0, 0.5))
		bg_img = cv2.imread('bg/cam0_0_136.jpg')
		for index, product in enumerate(sorted_product_list):
			bg_img = imageconbinator.image_conbinate(bg_img, product.product_img, product.product_mask, product.con_bg_coordinate)
			imageconbinator.coordinate_finetune(index, sorted_product_list)
			# cv2.rectangle(bg_img, (product.start_x, product.start_y), (product.end_x, product.end_y), (0, 255, 0), 2)
			data = ['conbination_'+str(x)+'.jpg', [[product.start_x, product.start_y],[product.end_x, product.end_y]], product.product_class]
			w.writerow(data)
			# print (product.start_y, product.start_x, product.end_y, product.end_x)
		print('conbination_'+str(x)+'.jpg' + 'finished!')
		if x < 1500:
			# cv2.imwrite('result/ridgid_proudct_20190801/train/conbination_'+str(x)+'.jpg', bg_img)
			cv2.imwrite('result/no_ridgid_product_20190806/train/conbination_'+str(x)+'.jpg', bg_img)
		else:
			cv2.imwrite('result/no_ridgid_product_20190806/val/conbination_'+str(x)+'.jpg', bg_img)
	# #f1 = open("product_combination_test/frcnn_500.csv", "w", newline='')
	# #f2 = open("product_combination_test/frcnn_1000.csv", "w", newline='')
	# f3 = open("result/product_combination_test_6/frcnn_3000.csv", "w", newline='')
	# #w1 = csv.writer(f1)
	# #w2 = csv.writer(f2)
	# w3 = csv.writer(f3)
	# circle_class = [0, 1, 2, 3, 4, 9]
	# for x in range (0,3000):		
	# 	bg = cv2.imread('bg.jpg')
	# 	if x < 1500:
	# 		class_num = int(x/150)
	# 		#print (class_num)
	# 		result ,csv_list = imageconbinator.image_conbination(True, bg, class_num)
	# 		cv2.imwrite('result/product_combination_test_6/train/conbination_'+str(x)+'.jpg',bg)
	# 		for csv in csv_list:
	# 			data = ['conbination_'+str(x)+'.jpg', csv[0], csv[1]]
				
	# 			w3.writerow(data)
	# 	elif x < 2500:
	# 		result ,csv_list = imageconbinator.image_conbination(True, bg, -1)
	# 		cv2.imwrite('result/product_combination_test_6/train/conbination_'+str(x)+'.jpg',bg)
	# 		for csv in csv_list:
	# 			data = ['conbination_'+str(x)+'.jpg', csv[0], csv[1]]
	# 			w3.writerow(data)
	# 	else:
	# 		#print (class_num)
	# 		result ,csv_list = imageconbinator.image_conbination(True, bg, -1)
	# 		cv2.imwrite('result/product_combination_test_6/val/conbination_'+str(x)+'.jpg',bg)
	# 		for csv in csv_list:
	# 			data = ['conbination_'+str(x)+'.jpg', csv[0], csv[1]]
	# 			w3.writerow(data)
		
	# 	print ('image: '+str(x)+' finish!')
